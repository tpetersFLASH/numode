\chapter{Boundary Value Problem}
\label{chapter:rwa}
\mode*
\begin{intro}
This chapter deals with problems of a fundamentally different type than
the problems we examined in chapter~\ref{cha:awa}
and which we solved with previous numerical methods, namely the boundary
value problems.
The representation we use here is based primarily on
~\cite{DeuflhardBornemann08} and~\cite{Rannacher12}.
\end{intro}

\begin{definition}
  \index{BVP|see{boundary value problem}} 
  A \textbf{boundary value problem}\defindex{Boundary value problem} (BVP) is a
	differential equation problem of the form: Find $u:[a,b]\to \R^d$, 
	such that
  \begin{subequations}
    \label{eq:rwa:1}
    \begin{xalignat}{2}
      \label{eq:rwa:2}
      u'(t) &= f\bigl(t,u(t)\bigr)
      & t &\in (a,b) \\
      \label{eq:rwa:3}
      r\bigl(u(a), u(b)\bigr) &= 0.
    \end{xalignat}
  \end{subequations}
\end{definition}


\begin{remark}
	Since the boundary value problems are furthered at two different
	points in time, the concept of local solutions from definition  
  ~\ref{def:awa:local solution} is here not applicable. 
	Thus, tricks as going forward from interval to interval, which is
	for instance done with Euler's method in the proof of Péano's theorem,
	are here not applicable. 
	For this reason nothing can be concluded with the local properties
	of the right hand side $f$ at the points $a$ and
  $b$. In fact, it is just possible in a few special cases to conclude that
	a solution exists.
\end{remark}

\input{derivatives}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Theory of boundary value problems}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{remark}
	The very general boundary condition (BC) \index{Boundary condition}~\eqref{eq:rwa:3} 
	has usually more simple forms. Often it is a \textbf{linear} \defindex{Linear boundary condition} condition 
	which we can note in the following form
  \begin{gather}
    \label{eq:rwa:4}
    \rwaa u(a) + \rwab u(b) = g
  \end{gather}
  with $d\times d$ matrices $\rwaa$ and $\rwab$ as well as a vector $c\in
  \R^d$. An other very common case is the one of 
  \textbf{separated}
  \index{Boundary condition!separated}
  \index{Separated BC}
  boundary conditions, which has the form
  \begin{xalignat}{2}
    \label{eq:rwa:5}
    r_a\bigl(u(a)\bigr) &= 0,
    &
    r_b\bigl(u(b)\bigr) &= 0,
    \\\intertext{or}
    \rwaa u(a) &= g_a,
    &
    \rwab u(b) &= g_b.
  \end{xalignat}
\end{remark}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{remark}
	As said before, it is only in special cases possible to get a satisfying 		
	solution theory for BVP. 
	As a consequence we now turn our attention to
	a ``restricted'' solution theory.
	The here posed question reads: Assume a solution of the problem
	can be found. Which further conditions are necessary to
	achieve a well-posedness of the problem in terms of Hadamard
  (definition~\vref{def:Hadamard}). \\
	The key is the following definition which grants us the possibility of 
	the approximation of a solution, at least after a quantification of
	the neighborhood.
\end{remark}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
\begin{definition}
  \defindex{Solution!local unique (BVP)}
  A solution $u(t)$ of the BVP~\eqref{eq:rwa:1} is called \textbf{local 
    unique} \defindex{Local
    unique solution (BVP)} or \textbf{isolated}\defindex{Isolated solution}, if there is no second solution
  $v(t)$ of the BVP, which is arbitrary close to $u(t)$. In
  formulaic language: it exists an $\epsilon>0$, such that it holds
  \begin{gather*}
    \max_{t\in [a,b]}|u(t)-v(t)| < \epsilon
    \qquad\Rightarrow\qquad
    u(t) = v(t) \quad \forall t\in [a,b].
  \end{gather*}
\end{definition}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{lemma}
  \label{lemma:rwa:E}
  Let be $f(t,u)$ continuous in $t$ and continuously differentiable in $u$. 
	Let additionally be $r(x,y)$ continuously differentiable and 
	set\footnote{This definition is consistent with the usage of the matrix $\rwaa$
    and $\rwab$ in equation~\eqref{eq:rwa:4}.}
  \begin{gather}
    \label{eq:rwa:9}
    \rwaa = \left.\frac{\partial r(x,y)}{\partial
        x}\right|_{x=u(a),y=u(b)},
    \qquad
    \rwab = \left.\frac{\partial r(x,y)}{\partial
        y}\right|_{x=u(a),y=u(b)}.
  \end{gather}
  Let be $u(t)$ a continuously differentiable solution of the
	BVP~\eqref{eq:rwa:1}.  
	Then the derivation of the boundary condition
  $r(u(a),u(b))$ with respect to the function value $u(t)$ in the inside
	of the interval $[a,b]$ leads to
  \begin{gather}
    \label{eq:rwa:12}
    E(t)
    := \frac{\partial r\bigl(u(a),u(b)\bigr)}{\partial u(t)}
    = \rwaa \fundamental(a;t) + \rwab \fundamental(b;t).
  \end{gather}
\end{lemma}

\begin{proof}
  We consider the solution $u(\tau;s)$ of the BVP at first as solution of 
	the IVP with initial value $t$
  \begin{gather*}
    \frac{\partial}{\partial \tau} u(\tau;s) = f\bigl(\tau,u(\tau;s)\bigr), \qquad u(t) = s.
  \end{gather*}
  We have tautological $u(a;s) = u(a)$ and $u(b) = u(b;s)$, as we
  choose $s=u(t)$. 
	In fact, we do not introduce a new function but rather a new parameter $s$, 	
	in respect to which we want to derive.
	The derivative of the boundary condition can therefore be written as
  \begin{align*}
    \frac{\partial r\bigl(u(a),u(b)\bigr)}{\partial u(t)}
    &=
    \frac{d r\bigl(u(a;s),u(b;s)\bigr)}{d s}
    \\
    &= \rwaa \frac{\partial u(a;s)}{\partial s}
    + \rwab \frac{\partial u(b;s)}{\partial s}
    \\
    &= \rwaa \fundamental(a;t) + \rwab \fundamental(b;t).
  \end{align*}
\end{proof}


\begin{theorem}(Local uniqueness)
  \label{theorem:rwa:uniqueness}
  \index{Uniqueness!local} 
  \index{Local uniqueness BVP}
	Is under the assumption of lemma~\ref{lemma:rwa:E} the matrix $E(t)$
  regular for a $t\in [a,b]$, then it is regular for all $t\in
  [a,b]$ and the solution $u(t)$ is local unique.
\end{theorem}

\begin{proof}
  Let primarily be $E(t)$ regular. Then we have for $\tau \neq t$:
  \begin{align*}
    E(\tau) &= \rwaa \fundamental(a;\tau) + \rwab \fundamental(b;\tau)
    \\
    &= \rwaa \fundamental(a;t)\fundamental(t;\tau) + \rwab
    \fundamental(b;t)\fundamental(t;\tau)
    = E(t) \fundamental(t;\tau),
  \end{align*}
  where the first factor is regular through assumption, 
	the second one as a fundamental matrix of a linear ODE.
	For sufficiently small perturbations $\delta u$ of $u(t)$ 
	of an arbitrary point in time $t$,
  we then obtain for perturbations $\delta r$ of $r\bigl(u(a),u(b)\bigr)$ the
 	following:
  \begin{gather*}
    \delta r \approx \frac{\partial r\bigl(u(a),u(b)\bigr)}{\partial
      u(t)}\delta u
    = E(t) \delta u \neq 0.
  \end{gather*}
  For this reason $u(t)$ has to be local unique.
\end{proof}


Now that we examined the local uniqueness of the solution,
it yet remains to show the continuous dependency of data (stability).  
Here we are in particular interested, in analogy to the 
stability theorem, in the derivative
of the solution at time $t$ after perturbations of values on the boundary. 
For this purpose we have:

\begin{theorem}[Conditioning of the BVP]
  \label{theorem:rwa:condition}
  \index{Conditioning!BVP}
	Let the assumptions of lemma~\ref{lemma:rwa:E} hold and let be
  $u(t)$ the solution of the BVP~\eqref{eq:rwa:1}. 
	Furthermore for the solution it holds
  \begin{gather*}
    u(a) = x\quad \text{and} \quad u(b) = y.
  \end{gather*}
	Then we get the following relations for perturbations of the boundary values $x$ and $y$ 
  \begin{gather}
    \label{eq:rwa:20}
    \frac{\partial u(t)}{\partial x} = E^{-1}(t) \rwaa,
    \qquad
    \frac{\partial u(t)}{\partial y} = E^{-1}(t) \rwab,
  \end{gather}
\end{theorem}

\begin{proof}
	We demonstrate the proof for the derivative with respect to the
	left boundary value, because obviously the second equation 
	can be gained in the same way.
	With the chain rule we obtain
  \begin{gather*}
    \frac{\partial u(t)}{\partial x}
    = \frac{\partial u(t)}{\partial r(x,y)}
    \frac{\partial r(x,y)}{\partial x}
  \end{gather*}
  The second derivative is according to~\eqref{eq:rwa:9} $\rwaa$. For
  the first one we notice that $E(t)$ is the derivative of the
	backwards mapping in terms of the definition of equation~\eqref{eq:rwa:12}. 	
	Through application of the theorem of implicit functions we obtain the 
	result.
\end{proof}

\begin{todo}
  Stabilität bzgl. $f$ aus~\cite{DeuflhardBornemann08}.
\end{todo}

\begin{remark}
  The theorems~\ref{theorem:rwa:uniqueness}
  and~\ref{theorem:rwa:condition} represent the verification of the 
	second and third Hadamard conditions\index{Hadamard conditions}. 
	Therewith the existence of a solution for the BVP is not ensured indeed,
	but it is guaranteed, that if such a solution exists, it can be 
	approximated.
\end{remark}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{definition}
  A BVP~\eqref{eq:rwa:1} is called linear, if the right hand side 
  $f$ as well as the boundary condition are linear in $u$. It has the form:
  find $u:[a,b]\to \R^d$, such that
  \begin{subequations}
    \label{eq:rwa:6}
    \begin{xalignat}{2}
      \label{eq:rwa:7}
      u'(t) &= A(t)u(t) + f(t) & \forall t &\in (a,b) \\
      \label{eq:rwa:8}
      \rwaa u(a) + \rwab u(b) &= g.
    \end{xalignat}
  \end{subequations}
\end{definition}

\begin{corollary}
  The linear BVP~\eqref{eq:rwa:6} has an unique solution $u(t)$ for arbitrary 	
	data $f(t)$ and $g$ if and only if
  the $d\times d$ matrix
  \begin{gather*}
    E(a) = \rwaa + \rwab \fundamental(b;a)
  \end{gather*}
  is regular.
\end{corollary}

\begin{proof}
	Primarily we observe that under the condition for $E(a)$ a solution
  $u(t)$, if it exists, is local unique. 
	Thus it is global unique, because if there would be a second solution
	 $v(t)$, then all linear combinations $w(t) =
  \theta u(t)+(1-\theta) v(t)$ would be solutions too, due to linearity.
	Therefore we get a contradiction to local uniqueness with $\theta \to 0$ and
  $\theta \to 1$.
\end{proof}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Shooting methods}
\subsection{Single shooting method}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{example}
	We illustrate the shooting method on a simple, scalar
  example
  \begin{gather*}
    u'' = -g, \quad u(0) = 0, \quad u(1) = 0.
  \end{gather*}
	What we can solve is the IVP
  \begin{gather*}
    u'' = -g, \quad u(0) = 0, \quad u'(0) = s.
  \end{gather*}
	The latter has a unique solution u(t;s) for each initial value
  $s$. Now it is our task to find a $s^*$, such that
  $u(1;s^*) = 0$. With other words, we search for a root of the function
  \begin{gather*}  
    F(s) = u(1;s).
  \end{gather*}
	This can be done with an arbitrary, convergent iteration method.
	For an example with the Bisection method. 
	Of course the Newton method would be a better choice 
	but for that we need to calculate the derivatives of $F$.
	This can be achieved with theorem~\ref{theorem:awa:conditioning}
	by calculating the fundamental matrix $\fundam$.
\end{example}

\begin{definition}
  \index{Shooting method!single}
  The \textbf{single shooting method} \defindex{Single shooting method} \footnote{Ger.:
  Einfachschießverfahren} 
  for the BVP~\eqref{eq:rwa:1} reads as follows: find an
  initial vector $s\in \R^d$, such that the solution $u(t) = u(t;s)$ of the IVP
  \begin{gather*}
    \tfrac\partial{\partial t} u(t;s) =f\bigl(t,u(t;s)\bigr), \qquad u(a;s) = s 
  \end{gather*}
  satisfies the boundary conditions \eqref{eq:rwa:3}.
\end{definition}

\begin{remark}
	The task of the shooting method is solved normally with the help
	of Newton's method, which searches for a root of the function
  \begin{gather}
    \label{eq:rwa:11}
    F(s) = r\bigl(s, u(b;s)\bigr)
  \end{gather}
	For Newton's method we require the partial derivatives
  \begin{gather*}
    \frac\partial{\partial s_i} F(s) =
    \partial_1 r\bigr(s,u(b;s)\bigr)
    + \partial_2r\bigr(s,u(b;s)\bigr) \frac\partial{\partial s_i} u(b;s)
  \end{gather*}
  For linear and separate boundary conditions this leads to a significantly 	
	more readable form: 
  \begin{gather*}
    \frac\partial{\partial s_i} F(s) = \rwaa s + \rwab \frac\partial{\partial s_i} u(b;s)
  \end{gather*}
	In both cases we need the matrix $\fundamental(b;a)$ of 
  derivatives of the solution at point $b$ with respect to the
	initial values. 
\end{remark}

\begin{algorithm}
  The single shooting method with \putindex{Newton method} consists of the
	following steps:
  \begin{enumerate}
  \item Start with a initial value $s^{(0)}\in \R^d$.
  \item To given $s^{(n)}$, solve the BVP and the associated variation equation
    \begin{xalignat*}{2}
      \tfrac\partial{\partial t}u(t;s^{(n)}) &= f\bigl(t,u(t;s^{(n)})\bigr) \\
      \tfrac\partial{\partial t}\fundamental(t;t_0)
      &= \nabla_uf\bigl(t,u(t;s^{(n)})\bigr)\fundamental(t;t_0)
    \end{xalignat*}
  \item Set
    \begin{gather}
      \label{eq:rwa:14}
      s^{(n+1)} = s^{(n)}
      - \bigl(\rwaa + \rwab \fundamental(b;a)\bigr)^{-1}
      r\bigl(s,u(t;s^{(n)})\bigr)
    \end{gather}
  \item Stop the iteration if the value
    $r\bigl(s,u(t;s^{(n+1)})\bigr)$ is sufficiently small.
  \end{enumerate}
\end{algorithm}

\begin{corollary}
  For linear BVP~\eqref{eq:rwa:6} the initial value $s$, for
  which $u(t;s)$ solves the BVP, results as solution of a linear system of 		
	equations
  \begin{gather}
    \label{eq:rwa:13}
    \bigl(\rwaa + \rwab\fundamental(b;a)\bigr) s = g - \rwab u(b;0).
  \end{gather}
\end{corollary}

\begin{proof}
	For linear equations we know that the Newton's method converges in one step,
	for arbitrary initial values.
	We can use this to execute the upper algorithm with initial value
	 $s^{(0)} = 0$.
	Under application of $r(u(a),u(b)) = B_a u(a) + B_b u(b) -
  g$ we obtain the upper formula by inserting in~\eqref{eq:rwa:14}.
\end{proof}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Multiple shooting method}
 \index{Shooting method!multiple}
\begin{todo}
multiple shooting method
\end{todo}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "notes"
%%% End: 
