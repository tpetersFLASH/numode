\chapter{Implicit One-Step Method}
\mode*
\begin{intro}
	The previous chapter dealt basically with the analysis of methods,
	which solve the IVP by discretization with shrinking step size $h$.
	We could gain a priori error estimates from consistency and stability
	for sufficient small $h$.   	
	Since all of this error estimates are based on Gronwall's inequality,
	they contain a term of the form $e^{Lt}$ which increases fast with
	increasing time intervals. Therefore the analyzed methods may be 
	unsuitable for long-term prognosis. 
	At any rate the obtained estimates are unsuitable.
  
	On the other hand our solar system, for instance,  moves on stable 
	orbits since several billion years and we do not observe a exponential
	increasing of velocities. Thus, there are in fact applications
	for which the simulation of long time periods are worth it
	and for which a exponential error term would be extremely disturbing.
	
	This chapter deals at first with suited criteria for the continuous 
	problem and subsequently with methods which have similar properties.
\end{intro}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Monotone initial value problem}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{example}
  \label{ex:linear-model}
  We consider for $\lambda \in \C$ the linear initial value problem
  \begin{equation}
    \begin{split}
      \label{eq:impl:testproblem}
      u' & = \lambda u\\
      u(0) & = 1.
    \end{split}
  \end{equation}
  The (complex) solution of this problem is $u(t) = e^{\lambda t}$. 
  The behavior of $u(t)$ for $t\to\infty$ is the result of
  \begin{gather}
    \label{eq:impl:cases}
    \begin{aligned}
      \Re(\lambda) < 0 & :\qquad& u(t) &\to 0 \\
      \Re(\lambda) = 0 & :\qquad& |u(t)| &= 1 \\
      \Re(\lambda) > 0 & :\qquad& u(t) &\to \infty
    \end{aligned}
  \end{gather}
  Moreover, the solution is bounded for $\lambda$ with non-positive
  real part for all points in time $t$.
\end{example}

\begin{remark}  
  Gronwall's inequality provides us for the above IVP the estimation
  \begin{gather*}
    |u(t)| \le e^{|\lambda|t} |u(0)|
  \end{gather*}
	by applying the comparison function $v(t) \equiv 0$.
	As soon as $\lambda \neq 0$ has a non-positive real part, 
	this estimate is still correct but very pessimistic and therefore
	useless. 
	Since problems with bounded long-term behavior are quite important
	in applications, we will have to deal with an improved notation of 
	stability.	
\end{remark}

\begin{remark}
	Since we deal in the following again and again with 	
	eigenvalues of real-valued matrices, we will always consider complex-valued IVP hereafter,
	due to the well known fact, that these eigenvalues can be complex.
\end{remark}

\begin{frame}
\begin{definition}
  \defindex{Lipschitz condition!one-sided}
  \defindex{One-sided Lipschitz condition}
  The function $f(t,y)$ satisfies on its domain $D \subset
  \R \times \C^d$ an one-sided Lipschitz condition if 
  the inequality
  \begin{gather}
    \label{eq:impl:Lipschitz}
    \Re \scal({f(t,y) - f(t,x)},y-x) \le \nu \norm{y-x}^2
  \end{gather}
  holds with a constant $\nu$ for all $(t,x),(t,y)\in D$. 
	Moreover such a function is called
  \textbf{monotonous} \defindex{Monotonous function} if $\nu=0$, thus 
  \begin{gather}
    \label{eq:impl:monoton}
    \Re \scal({f(t,y) - f(t,x)},y-x) \le 0.
  \end{gather}
\end{definition}
\end{frame}

\begin{remark}
	The term monotonous from the previous definition is consistent with
	the term \emph{monotonous decreasing}, which we know from
	real-valued functions. We can see this by observing  
  \begin{gather*}
    \bigl(f(t,y)-f(t,x)\bigr)(y-x) \le 0
    \quad \Leftrightarrow \quad f(t,y)-f(t,x) < 0.
  \end{gather*}
	for $y>x$.
\end{remark}

\begin{frame}
\begin{theorem}
  \label{theorem:implicit:monotonic}
  Let $u(t)$ and $v(t)$ be two solutions of the equation $u'=f(t,u)$ 
  with initial values $u(t_0) = u_0$ and $v(t_0) = v_0$, where
	the function $f$ is continuous and fulfills the one-sided
  Lipschitz condition~\eqref{eq:impl:Lipschitz}. Then we have for
  $t>t_0$:
  \begin{gather}
    \label{eq:implicit:3}
    \norm{v(t)-u(t)} \le e^{\nu(t-t_0)} \norm{v(t_0) - u(t_0)}.    
  \end{gather}
\end{theorem}
\end{frame}

\begin{proof}
  We consider the auxiliary function $m(t) = \norm{v(t)-u(t)}^2$ and
  its derivative
  \begin{align*}
    m'(t) &= 2\Re\scal({v'(t)-u'(t)},{v(t)-u(t)}) \\
    &= 2 \Re\scal({f\bigl(t,v(t)\bigr)-f\bigl(t,u(t)\bigr)},{v(t)-u(t)})
    \\
    &\le 2 \nu \norm{v(t)-u(t)}^2 \\
    &= 2 \nu m(t).
  \end{align*}
  According to Gronwall's inequality (lemma~\vref{lemma:Gronwall})
  we obtain for $t > t_0$:
  \begin{gather*}
    m(t) \le m(t_0) e^{2\nu(t-t_0)}.
  \end{gather*}
	Root extraction leads to the stability 
	estimation~\eqref{eq:implicit:3}.
\end{proof}

\begin{remark}
	Analog to example~\vref{ex:linear-model} we receive from the
  stability estimation, that for the difference of two solutions
  $u(t)$ and $v(t)$ of the differential equation $u'=f(t,u)$ we obtain 
	in the limes $t\to\infty$:
  \begin{gather}
    \label{eq:implicit:4}
    \begin{aligned}
      \nu < 0: & \qquad & \norm{v(t)-u(t)} &\to 0 \\
      \nu = 0: & \qquad & \norm{v(t)-u(t)} &\le \norm{v(t_0)-u(t_0)}
    \end{aligned}
  \end{gather}
\end{remark}

\begin{frame}
\begin{lemma}
  For the linear differential equation $u'=Au$ with $u(t)\in \C^d$ and
  a matrix function $A(t) \in \C^{d\times d}$ the one-sided
  Lipschitz condition~\eqref{eq:impl:Lipschitz} holds with the optimal
	constants $\nu = \max_{i=1,\dots,d} \Re(\lambda_i)$.
	Here one has to take the maximum over all points in time $t$.
	In accordance the right hand side is monotonous if and only if
	for all eigenvalues $\lambda_i$ of $A(t)$ it holds
  \begin{gather}
    \label{eq:implicit:10}
    \Re (\lambda_i) \le 0.
  \end{gather}
	This is the vector-valued form of example~\ref{ex:linear-model}. 
\end{lemma}
\end{frame}

\begin{proof}
  For the right hand side of the equation we have
  \begin{gather*}
    \Re \scal(A(t)y-A(t)x,y-x)
    \le \Re \frac{\scal(A(t)y-A(t)x,y-x)}{\norm{y-x}}\norm{y-x}
    \le \max_{i=1,\dots,d}\Re (\lambda_i) \norm{y-x}.
  \end{gather*}
  Hence, we obtain already $\nu \le \max_{i=1,\dots,d}
  \Re(\lambda_i)$. If we now insert for $x-y$ an eigenvector of 
	eigenvalue	$\lambda$ for which the maximum is accepted,
	then we obtain the equality and therefore $\nu = \max_{i=1,\dots,d}
  \Re(\lambda_i)$.
\end{proof}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Stiff initial value problems}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{example}
  \label{ex:impl:2}
  We consider the IVP
  \begin{gather}
    \label{eq:implicit:5}
    u' =
    \begin{pmatrix}
      -1 & 0 \\ 0 & -100
    \end{pmatrix}u,
    \qquad u(0) =
    \begin{pmatrix}
      1 \\ 1
    \end{pmatrix}.
  \end{gather}
  This has the solution
  \begin{gather*}
    u(t) =
    \begin{pmatrix}
      e^{-t} \\ e^{-100t}
    \end{pmatrix}.
  \end{gather*}
  We see, that the solution has a component which decreases slowly in 
	time with $e^{-t}$ and a second one, which decreases fast with
  $e^{-100t}$. If we apply Euler's method with step size $h$ 
	to this equation, then we obtain the method step
  \begin{gather*}
    y^{(n+1)} = y^{(n)} + h
    \begin{pmatrix}
      -1 & 0 \\ 0 & -100
    \end{pmatrix} y^{(n)}
    = \begin{pmatrix}
      1-h & 0 \\ 0 & 1-100h
    \end{pmatrix}y^{(n)}
  \end{gather*}
  with the solution
  \begin{gather*}
    y^{(n)} =
    \begin{pmatrix}
      (1-h)^n \\ (1-100h)^n
    \end{pmatrix}
  \end{gather*}
	Are we interested in the second solution component, the one which 
	decreases fast, then we would choose $h$ to be small.
	Moreover $h <1/100$. Therefore $y_n\to 0$ converges for $n\to\infty$,
	the first component slowly, the second one fast, just like the 
	solution $u(t)$ of the continuous solution.
	(Recall that for fix chosen step size $h$ the limits $t\to\infty$ 
	and $n\to\infty$ are equal.)
  
	If we are just interested in the first one, the slow component,
	then a considerably larger step size would be sufficient.
	Let's say $h=1/10$. For this step size the first solution
	component is still converging to zero with $y^{(n)}_1 = 0.9^n$. 
	For the second one we have however
	$|y^{(n)}_2| = |(-9)^n| = \infty$. Therefore the approximated 
	solution for this step size diverges for $n\to\infty$. 
	This stands very much in contrast to the behavior of the exact solution 
	$u(t)$ for $t\to\infty$.
\end{example}

\begin{remark}
	While it would have been of course possible to just ignore the second
	component in the previous example, that is not so easily accomplished
	in general. This is due to the fact that most solution components are
	connected through the equation.
	In such cases the step size of Euler's method has always to be orientated
	on the fast components. This can lead to a significant computing 
	overhead.
	Because of that reason we define in the following characteristic
	properties of such problems and develop to that specially adapted
	solution methods. 	
\end{remark}

\begin{frame}
\begin{definition}[Stiff IVP]
  \defindex{Stiff initial value problem}
  \defindex{Initial value problem!stiff}
  An initial value problem is called \define{stiff}, if it has
	the following characteristic properties:
  \begin{enumerate}
  \item The right hand side of the ODE is \putindex{monotonous}.
  \item The \putindex{time scales} on which the different
		solution components are evolving differ a lot
  \item The time scales which are of interest for the application 
	are way lager than the fastest time scales of the equation.
  \end{enumerate}
\end{definition}
\end{frame}

\begin{remark}
	Even though we used the term definition, it will not have escaped 
	the	attentive reader, that the notion of the stiffness of an IVP 
	has	something vague or even inaccurate about it. In fact that is
  due to the very nature of the problems and cannot be fixed.	
	Instead we are forced	to sharpen our understanding by means of a 
	few examples.
\end{remark}

\begin{remark}
	The third condition in the definition of stiffness is rather 
	rare to find in the literature, but it is in general implicitly 
	assumed	by the discussion for time step methods for stiff IVP.
	It is important though to envision oneself, that the so far presented
	methods do not lead to problems if one is interested in a good
	resolution for the fastest time scales.  
\end{remark}

\begin{example}
  \label{ex:impl:2a}
	First of all we will have a look at equation~\eqref{eq:implicit:5} 
	in example~\ref{ex:impl:2}. The first condition of the stiffness definition
  is fulfilled. Since the decrease to $1/e$ occurs by the 
	first component at $t=1$ and by the second one already at $t=1/100$, 	
	we take the second condition as well for granted.
  
	According to the discussion of example~\ref{ex:impl:2}, 
	the third condition depends on the purpose of the computation.  
	If one wants to compute the solution at time $t=1/100$,
	one would not denote the problem as stiff. As one is interested
	on the solution at time $t=1$, on which the second component
	with $e^{-100}$ is already below the common machine accuracy,
	the problem is stiff indeed. Here we have seen that Euler's method
	requires disproportionately small time steps.
\end{example}

\begin{remark}
  \label{remark:impl:1}
	The definition of stiffness and the discussion of the
	examples reveal that numerical methods are needed,
	which are not just convergent for time steps $h\to 0$
	but also for given step size $h$, even in the presence of 
	time scales clearly underneath of $h$. For this scenario the methods 
	still have to produce solutions with correct limit behavior for 
	$t\to\infty$. The rest of this chapter deals with the derivation of suitable properties 
	and methods.
\end{remark}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{A-stability}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

In this section we will deal at first with the investigation 
of desired properties of methods, which are suitable for stiff, linear,
autonomous IVP. Thus, we are working with problems of type

\begin{gather}
  \label{eq:implicit:6}
  u'=Au \qquad u(t_0) = u_0.
\end{gather}

\begin{remark}
	We are interested for cases in which the IVP~\eqref{eq:implicit:6} 
	is stiff. From the conditions for stiffness we derive
	the following problem characteristics:
  \begin{enumerate}
  \item All eigenvalues of the matrix $A$ lie in the left half-plane
    of the complex plane. With~\eqref{eq:impl:cases} all solutions 
		are bounded for $t\to\infty$.
  \item There are eigenvalues close to zero and eigenvalues with
		a large negative real part.
  \item We are interested in time spans which make it necessary,
	that the product $h\lambda$ of a time step and an arbitrary 
	eigenvalue, is allowed to be large. 
  \end{enumerate}
	For this case we now want to derive criteria for the boundedness
	of the discrete solution for $n\to\infty$.
\end{remark}

\begin{frame}
\begin{definition}
  \defindex{Stability function} The \textbf{stability function} 
	\footnote{German: Stabilitätsfunktion/Verstärkungsfunktion}
	$R(h \lambda)$ results from the application of one step of the
  method to the test problem~\eqref{eq:impl:testproblem}: 
  \begin{equation}
    y_1 = R(h \lambda) u_0
  \end{equation}
  We obtain:
  \begin{equation}
    y^{(n)} = R(h \lambda)^n u_0
  \end{equation}
\end{definition}
\end{frame}

\begin{example}
	The stability function of the explicit Euler method is derived as follows:

  \begin{equation}\begin{split}
  \label{eq:impl:stabil:expleuler}
    y_1 &= y_0 + h \lambda y_0 = (1 + h \lambda) y_0 \\
    \Rightarrow R(h \lambda) &= 1 + h \lambda \\
    R(z) &= 1 + z \\
  \end{split}\end{equation}
\end{example}


\begin{example}
	The stability function of the implicit Euler method is derived as follows:

  \begin{equation}\begin{split}
  \label{eq:impl:stabil:impleuler}
    y_1 &= y_0 + h f(t_1, y_1) \\
    y_1 &= y_0 + h \lambda y_1 \\
    (1 - h \lambda) y_1 &= y_0 \\
    \Rightarrow R(h \lambda) &= \frac{1}{1- h \lambda} \\
    R(z) &= \frac{1}{1-z} \\
  \end{split}\end{equation}
\end{example}

\begin{frame}
\begin{definition}
 The \textbf{stability region}\defindex{Stability region} of a method is the set 
  \begin{gather}
    S = \{ z \in \C | \ | R(z) | \leq 1 \}
  \end{gather}

  Thereby $R(z)$ is the stability function of the method.
\end{definition}
\end{frame}


\begin{example}
	The stability region for the explicit Euler is a circle with radius
	1 around the point (-1,0) in the complex plane
  (cf.~\eqref{eq:impl:stabil:expleuler}). %% anschauliches Bild?

	The stability region for the implicit Euler is the whole complex
	plane without a circle with radius 1 around the point (1,0)
  (cf.~\eqref{eq:impl:stabil:impleuler}). %% anschauliches Bild?
\end{example}

\begin{frame}
\begin{definition}[A-stability]
  A method is called \define{A-stable}, if its stability region contains
  the left half-plane of $\C$, hence
  \begin{gather}
    \{ z \in \C | \ Re(z) \leq 0 \} \subset S
  \end{gather}
\end{definition}
\end{frame}

% \begin{remark}
%   Der Begriff A-stabil wurde von Dahlquist bewusst neutral
%   gewählt. Insbesondere steht also A-Stabilität
%   \underline{\textbf{nicht}} für asymptotische Stabilität.
% \end{remark}

\begin{frame}
\begin{theorem}
  Let be $\left\{y^{(k)}\right\}$ the sequence which generates an 
	A-stable method
  of step size $h$ for the linear IVP
  \begin{gather*}
    u'=Au, \qquad u(t_0) = u_0
  \end{gather*}
  with the initial value $y^{(0)} = u_0$. If additionally all
  eigenvalues of $A$ have a non-positive real part, then the sequence
 	members $y^{(k)}$ are uniformly bounded for all $h$.
\end{theorem}
\end{frame}

\begin{todo}
\begin{proof}
	%$\left\{y^{(k)}\right\}$ generates an A-stable method.  \\
	%\begin{align*}
		%\Rightarrow~ &\{ z \in \C | \ Re(z) \leq 0 \} \subset S = \{ z \in \C | \ | R(z) | \leq 1 \}, \\
	 	%&\text{where R(z) is the stability function of the method}\\
		%\Rightarrow~ &\{ z=\lambda h \in \C ~ \forall h | \lambda ~\text{eigenvalue of A} \} \subset S, \\
		%&\text{because}~ Re(\lambda)\leq 0.\\
		%\Rightarrow~ &R(h \lambda)\leq 1 ~ \forall h ~\text{and}~ \lambda ~\text{eigenvalue of A}\\	
		%\Rightarrow~ &|y^{(k)}|=|R(h \lambda)^k u_0|\leq|R(h \lambda)|^k |u_0|\leq|u_0| < \infty ~ \forall h,~ \forall k
	%\end{align*}
\end{proof}
\end{todo}

\begin{frame}
\begin{theorem}
  \label{theorem:implicit:a-stability-erk}
	No explicit Runge-Kutta method is $A-stable$ in accordance with 
	definition~\vref{def:erk}.
\end{theorem}
\end{frame}

\begin{proof}
	We show that $R(z)$ is a polynomial for such methods.
	The assumption follows directly because it is known for polynomials,
	that the absolute value of its values goes to infinity, if the
	absolute value of the argument goes to infinity.
  
  From the equation~\eqref{eq:explicit:1b} follows $k_i = \lambda
  g_i$. If we insert that into the equation~\eqref{eq:explicit:1a},
  we obtain
  \begin{gather*}
    g_i = y_0 + h \sum_{j=1}^{i-1} \rka_{ij}k_j = y_0 + h\lambda
    \sum_{j=1}^{i-1} \rka_{ij} g_j.
  \end{gather*}
  With $g_1 = y_0$ and $z=h\lambda$ one has
  \begin{align*}
    g_2 &= y_0+ \rka_{21} z y_0 = (1+\rka_{21}z) y_0\\
    g_3 &= y_0 +  \rka_{32} z g_1 =  y_0 +  \rka_{32} z (1+\rka_{21} z) y_0 =
    (1+\rka_{32} z(1+\rka_{21} z)) y_0.
  \end{align*}
	Therefore one shows easily per induction that $k_j$ results as
	multiplication of a polynomial of order $j-1$ with $y_0$. 
  With formula~\eqref{eq:explicit:1c} we have that $R(z)$ is a
  polynomial of order $\rks-1$.
\end{proof}

\begin{frame}
\begin{definition}
  A RKM is called \define{B-stable}, if for monotonous functions with
  arbitrary initial value $y_0$ and $z_0$ it holds:
  \begin{equation}
    | y_1 - z_1 | \leq | y_0 - z_0 |
  \end{equation}
  independent of $h$.
\end{definition}
\end{frame}

\begin{frame}
\begin{theorem}
  Let be $\left\{y^{(k)}\right\}$ the subsequence which generates a 
	B-stable method
  of step size $h$ for the IVP
  \begin{gather*}
    u'=f(t,u), \qquad u(t_0) = u_0
  \end{gather*}
  with initial values $y^{(0)} = u_0$. If the right hand side
  $f$ is monotonous, then the subsequence members $y^{(k)}$ 
	are uniformly bounded for each choice of $h$.
\end{theorem}
\end{frame}


\begin{todo}
\begin{proof}
\end{proof}
\end{todo}

\begin{corollary}
 	A B-stable method applied to a linear differential equation
	is A-stable.
\end{corollary}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{General Runge-Kutta methods}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{intro}
	In the last chapter we got to know explicit Runge-Kutta methods.
	They distinguish itself from implicit methods
	through the fact that the coefficients $k_i$ as well as 
	the result of the time step $y_{k+1}$ can be calculated without
	solving a linear or non-linear equation system, but rather by
	linear combinations of function evaluations.
 	According to theorem~\ref{theorem:implicit:a-stability-erk} 
	on the other hand this methods can not be A-stable, therefore
	we will extend this methods with the so-called implicit 
	Runge-Kutta methods. In fact we will see that 
	the additional effort for the execution of the method
	is going to be rewarded with A-stability.
\end{intro}

\begin{frame}
\begin{definition}
  \label{def:implicit:rk}
  A \define{Runge-Kutta method} is a one-step method of the form
  \begin{subequations}
    \label{eq:implicit:1}
    \begin{xalignat}{2}
      \label{eq:implicit:1a}
      \rkg_i &= y_0 + h \sum_{j=1}^{\rks} \rka_{ij} k_j
      & i &= 1,\dots,\rks
      \\
      \label{eq:implicit:1b}
      k_i &= f(t_0+h \rkc_i, \rkg_i)
      & i &= 1,\dots,\rks
      \\
      \label{eq:implicit:1c}
      y_1 &= y_0 + h \sum_{i=1}^{\rks} \rkb_i k_i
    \end{xalignat}
  \end{subequations}
  The method is called
  \begin{description}
  \item[\textbf{ERK}] explicit Runge-Kutta method if $\forall 
    i \leq j: \ \rka_{ij} = 0$
    \index{Runge-Kutta method!explicit (ERK)|textbf}
  \item[\textbf{DIRK}] diagonal implicit Runge-Kutta method if $\forall i<j: \ \rka_{ij} = 0$
    \index{Runge-Kutta method!diagonal implicit (DIRK)|textbf}
    \index{Diagonal implicit (DIRK)}
    \index{DIRK|see{Runge-Kutta method}}
  \item[\textbf{SDIRK}] "`singly diagonal implicit"'
    Runge-Kutta method if it is diagonal implicit and
    additional $\forall i,j: \ \rka_{ii} = \rka_{jj}$
    \index{Runge-Kutta method!singly diagonal implicit (SDIRK)|textbf}
    \index{SDIRK|see{Runge-Kutta method}}
  \item[\textbf{IRK}] implicit Runge-Kutta method in all other cases.
    \index{Runge-Kutta method!implicit (IRK)|textbf}
    \index{IRK|see{Runge-Kutta method}}
  \end{description}
\end{definition}
\end{frame}

\begin{example}[Two-stage SDIRK]
  Both SDIRK methods in table~\ref{tab:SDIRK2} are of 
  order three
  \begin{table}[htp]
    \centering
  \begin{gather}
    \def\arraystretch{1.5}
    \begin{array}{c|cc}
      \frac12 - \frac{\sqrt{3}}{6} & \frac12 - \frac{\sqrt{3}}{6} & 0 \\
      \frac12 + \frac{\sqrt{3}}{6} & \frac{\sqrt{3}}{3} & \frac12 - \frac{\sqrt{3}}{6} \\
      \hline
      & \frac12 & \frac12
    \end{array}
    \qquad
    \begin{array}{c|cc}
      \frac12 + \frac{\sqrt{3}}{6} & \frac12 + \frac{\sqrt{3}}{6} & 0 \\
      \frac12 - \frac{\sqrt{3}}{6} & -\frac{\sqrt{3}}{3} & \frac12 + \frac{\sqrt{3}}{6} \\
      \hline
      & \frac12 & \frac12
    \end{array}
  \end{gather}
    \caption{Two-stage SDIRK method of order 3}
    \label{tab:SDIRK2}
  \end{table}
\end{example}

For existence and uniqueness of solutions of diagonal implicit 
Runge-Kutta methods as well as for the conclusion of
convergence through consistency, we refer to~\cite[Satz 2.2]{Rannacher12}.   

\begin{todo}
In the following we want to discuss under which conditions a solution 
for our IRK exists and is unique.
For that we will need the help of some definitions:

\begin{frame}
\begin{definition}
	We consider the inner product $\scal(u,v)_D = u^T Dv$,
  where $D$ is a diagonal matrix with the entries $d_i > 0$. 
	We now denote with $\alpha_D(A^{-1})$ the largest number
  $\alpha$ for which holds
  \begin{gather}
    \scal(u,A^{-1}u)_D \geq \alpha \scal(u,u)_D \qquad \forall u \in \R^\rks
  \end{gather}

  Futhermore we set
  \begin{gather}
    \alpha_0 (A^{-1}) = \sup_{D>0} \alpha_D (A^{-1})
  \end{gather}
\end{definition}
\end{frame}

\begin{frame}
\begin{theorem}[Existence of a solution for IRKs]
  Let be f continuously differentiable and it satisfies the 
	one-sided Lipschitz condition with the Lipschitz constant $L$. If the
  Runge-Kutta matrix $A$ is invertible and satisfies the condition
  \begin{gather}
    \label{eq:implicit:cond}
    hL < \alpha_0 (A^{-1}),
  \end{gather}
  then the nonlinear system~\ref{eq:implicit:1a} has a solution
  $(\rkg_1,...,\rkg_\rks)$.
\end{theorem}
\end{frame}

\begin{proof}
	%Crouzeix and Raviart (1980), Dekker (1982) and Crouzeix, Hundsdorfer & Spijker (1983)
	We refer to \cite[IV. Theorem 14.2]{HairerWanner10}.
\end{proof}

\begin{remark}
	Now we examine the perturbation susceptibility of the solution of the IRK.
	For this purpose we use as before the notation
  \begin{gather}
    \begin{array}{lll}
      || u ||_D & = \sqrt{u^TDu} = \sqrt{\langle u,u \rangle_D} & \ \ \ \forall u \in \R^\rks \\
      || g ||_D & = \sqrt{g^T(D\otimes I)g} & \ \ \ \forall g \in \R^{\rks n}
    \end{array}
  \end{gather}
\end{remark}

\begin{frame}
\begin{theorem}
  \label{theorem:implicit:perturb}
  Given $\rkg_i$ and $y_1$ as in ~\ref{eq:implicit:1a} and ~\ref{eq:implicit:1c} and the perturbed values $\hat{\rkg}_i$ and $\hat{y}_1$ are defined as follows
  \begin{gather}
    \begin{split}
      \hat{\rkg}_i & = y_0 + h \sum\limits_{j=1}^\rks \rka_{ij} f(x_0 + \rkc_j h, \hat{\rkg}_j) + \delta_i \\
      \hat{y}_1 & = y_0 + h \sum\limits_{j=1}^\rks \rkb_j f(x_0 + \rkc_j h, \hat{\rkg}_j) \\
    \end{split}
  \end{gather}

  If the Runge-Kutta matrix A is invertible, the differential equation 
	satisfies the one-sided Lipschitz condition and for a positive diagonal
	matrix D holds $hL<\alpha_D(A^{-1})$, then we have the estimates
  \begin{gather}
    \begin{split}
      ||\hat{\rkg}-\rkg||_D & \leq \frac{||A^{-1}||_D}{\alpha_D(A^{-1})-hL}||\delta||_D \\
      ||\hat{y}_1 - y_1|| & \leq ||b^TA^{-1}||_D \left( 1+\frac{||A^{-1}||_D}{\alpha_D(A^{-1})-hL} \right) ||\delta||_D \\
    \end{split}
  \end{gather}

  where $\rkg = (\rkg_1,...,\rkg_\rks)^T$, $\hat{\rkg}=(\hat{\rkg}_1,...,\hat{\rkg}_\rks)^T$ and $\delta=( \delta_1,...,\delta_\rks)^T$.
\end{theorem}
\end{frame}

\begin{proof}
	%Dekker (1984)
	We refer to \cite[IV. Theorem 14.3]{HairerWanner10}.
\end{proof}

\begin{frame}
\begin{theorem}[Uniqueness of the solution for IRKs]
  For a differential equation, which satisfies the Lipschitz conditions,
  the following holds true: a Runge-Kutta method has maximal one solution
	if its matrix $A$ is invertible 
  and the condition $~\ref{eq:implicit:cond}$ is satisfied.
\end{theorem}
\end{frame}

\begin{proof}
  Set $\delta = 0$ in $~\ref{theorem:implicit:perturb}$.
\end{proof}
\end{todo}

\begin{frame}
\begin{lemma}[Order condition for implicit Runge-Kutta methods]
% HNW p. 208
  \index{Order condition (IRK)}
  The quatratur formula with the quadrature points $\rkc_i$ and weights
  $\rkb_i$ is exact for polynomials of maximal degree $p_1$, if the
  conditions
  \begin{subequations}
    \label{eq:implicit:order}
    \begin{xalignat}{2}
      \label{eq:implicit:8}
      \sum_{i=1}^s \rkb_i \rkc_i^{q} &= \frac1{q+1}
      &&q=0,\ldots,p_1\\
      \intertext{are fulfilled. The quadrature formulas of the 
        form~\eqref{eq:implicit:1a} for the calculation of the intermediate
				values $g_i$
        are exact for polynomials of degree $p_2$, if the conditions}
      \label{eq:implicit:9}
      \sum_{j=1}^s \rka_{ij} \rkc_j^q &= \frac{\rkc_i^{q+1}}{q+1}
      &&q=0,\ldots,p_2; \quad i=1,\dots,\rks\\
      \intertext{are fulfilled. Moreover we introduce the conditions}
      \label{eq:implicit:11}
      \sum_{i=1}^s \rkb_i \rka_{ij} \rkc_i^q &=
      \frac{\rkb_j}{q+1}(1-\rkc_j^{q+1})
      &&q=0,\ldots,p_3; \quad j=1,\dots,\rks.
    \end{xalignat}
  \end{subequations}
\end{lemma}
\end{frame}

\begin{proof}
	For the proof we first observe, that
  \begin{gather*}
    \int_0^1 x^q \dx = \frac1{q+1}, \qquad
    \int_0^{c_i} x^q \dx = \frac{c_iq^{q+1}}{q+1}.
  \end{gather*}
	If we now insert the function $x$ at the places $c_i$ into the
	quadrature formula with the quadrature weights $\rkb_i$, then 
	we obtain~\eqref{eq:implicit:8}. Similarly we get ~\eqref{eq:implicit:9}, if we insert the value 
  $x^{q+1}/(q+1)$ at the places $c_i$ from the quadrature formula with
  weights $\rka_{ij}$ for $j=1,\dots,s$. In both cases 
	we carry this out for all momomials until the desired degree is 
	reached.
	Due to linearity of the formulas the exactness holds 
	for all polynomials until this degree.
\end{proof}

\begin{frame}
\begin{theorem}
  If for a Runge-Kutta method the condition~\eqref{eq:implicit:8}
  with $p_1\ge p-1$, condition~\eqref{eq:implicit:9} with $p_2\ge p/2$
  and condition~\eqref{eq:implicit:11} with $p_3 \ge p-p_2+1$ are satisfied,
  then the method is of order $p$.
\end{theorem}
\end{frame}


Without proof. For details we refer to~\cite[Theorem 7.4]{HairerNorsettWanner93}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Collocation methods}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{intro}
	An alternative to solving IVP in individual points in time, is to
	develop methods, which first approximate the solution function through
	a easier function. For example this could be a polynomial.
  
	Polynomials are especially suitable for the computation with computers.
	On the other hand we know already that they cause problems
	for instance by the interpolation of large intervals.
	Therefore we apply them not on the entire interval but rather
	on subintervals. The subintervals correspond obviously to our
	time steps, which we used until now.
\end{intro}

\begin{frame}
\begin{definition}
  A $\rks$-stage \textbf{collocation method} \defindex{Collocation method} with
  supporting points $c_1,\ldots,c_s$ defines a \textbf{collocation polynomial}\defindex{Collocation polynomial}
  $y(t)\in \pol_s$ through the conditional equation
  \begin{subequations}
    \label{eq:implicit:13}
    \begin{align}
      \label{eq:implicit:2}
      y(t_0) &= y_0 \\
      \label{eq:implicit:12}
      y'(t_0+\rkc_i h) &= f\bigl(t_0+\rkc_i h, y(t_0+\rkc_i h)\bigr)
      \quad i=1,\dots,s.
    \end{align}
		This yields to 
    \begin{gather}
      y_1 = y(t_0+h)
    \end{gather}
		as the result of the time step.
  \end{subequations}
\end{definition}
\end{frame}

\begin{frame}
\begin{lemma}
% HKW p. 212
  A $\rks$-stage collocation method with the points $\rkc_1$
  to $\rkc_\rks$ defines a Runge-Kutta method of
  definition~\ref{def:implicit:rk} with the coefficients $\rkc_i$ and
  \begin{gather}
  \label{eq:implicit:kolkoef}
    \rka_{ij} =  \int_0^{\rkc_i} L_j(t)\, \diffd t,
    \qquad
    \rkb_i =\int_0^1 L_j(t)\, \diffd t.
  \end{gather}
  Here is $L_j(t)$ Lagrange's interpolation polynomial to point $\rkc_j$
  and to the point set $\{\rkc_1,\dots,\rkc_\rks\}$:
  \begin{gather*}
    L_j(t) = \prod_{\substack{k=1\\k\neq j}}^\rks \frac{t-\rkc_k}{\rkc_j-\rkc_k}.
  \end{gather*}
\end{lemma}
\end{frame}

\begin{proof}
  The polynomial $y'(t)$ is of degree $\rks-1$ and therefore uniquely defined
	through $\rks$
  interpolation conditions in equation~\eqref{eq:implicit:12}. 
	We set $y'(x_0 + \rkc_i h) = f\bigl(t_0+\rkc_i h,
  y(t_0+\rkc_i h)\bigr) = k_i$, such that we have 
  \begin{gather}
    y'(x_0+th) = \sum\limits_{j=1}^\rks k_j \cdot L_j(t)
  \end{gather}
	with the Lagrange interpolation.

  By integration we obtain:
  \begin{gather}
    g_i = y(x_0 + \rkc_i h)
    = y_0 + h \int\limits_0^{\rkc_i} y'(x_0 + th) \dt
    = y_0+h \sum_{j=1}^s k_j \int_0^{\rkc_i} L_j(t)\dt,
  \end{gather}
  which defines the coefficients $\rka_{ij}$ by comparison with
	~\eqref{eq:implicit:1a}. 
	If we integrate until 1 instead of until $c_i$, then
  we get through comparison with~\eqref{eq:implicit:1c} the
  coefficients $\rkb_j$.
\end{proof}

\begin{frame}
\begin{lemma}
% HKW p. 212
  A implicit $\rks$-stage Runge-Kutta method of order
  $\rks$ or higher, for which the supporting points $\rkc_i$ differ
	pairwise, is a collocation method if and only if 
	condition~\eqref{eq:implicit:9} is satisfied with $p_2=s$. 
\end{lemma}
\end{frame}

\begin{proof}
	We see that~\eqref{eq:implicit:9} with $p_2=s$
  constitutes in $s^2$ interpolation conditions for $s^2$ coefficients
  $a_{ij}$. 
	Therefore this coefficients are defined uniquely. 
  On the other hand ~\eqref{eq:implicit:9} yields to
  \begin{gather*}
    \sum_{j=1}^s \rka_{ij} \rkc_j^q = \frac{\rkc_i^{q+1}}{q+1} =
    \int_{0}^{\rkc_i} t^q \dt.
  \end{gather*}
  As a consequence of linearity we have
  \begin{gather*}
    \sum_{j=1}^s \rka_{ij} p(\rkc_i) = \int_{0}^{\rkc_i} p(t)
    \dt,\qquad\forall p\in \Pol_s.
  \end{gather*}
  If one inserts Lagrange's interpolation polynomials $L_j(t)$ 
	into this, one exactly obtains the coefficients of  
	equation~\eqref{eq:implicit:kolkoef}.
\end{proof}

\begin{frame}
\begin{theorem}
  \label{theorem:implicit:colloction-order}
	For the pairwise different supporting points $\rkc_i$ of a 
  collocation method it is given
  \begin{gather}
    \label{eq:implicit:14}
    \pi(t) = \prod_{i=1}^\rks (t-c_i).  
  \end{gather}
  If $\pi(t)$ on the interval $[0,1]$ is orthogonal to all
  polynomials of degree $r-1$ , then the collocation 
	method~\eqref{eq:implicit:13} is of order $p=s+r$.
\end{theorem}
\end{frame}

\begin{proof}
We refer to~\cite[Theorem 7.8]{HairerNorsettWanner93}.
\end{proof}

\begin{corollary}
  $\rks$-stage collocation methods are at least of order
  $\rks$ and at most of order $2\rks$.
\end{corollary}

\begin{proof}
  The polynomial $\pi(t)$ in~\eqref{eq:implicit:14} is of degree
  $\rks$. As a result it can be orthogonal on all polynomials
	of degree $\rks-1$ in the best case.
	Otherwise it would be orthogonal to itself. 
	The transformed Legendre polynomial of degree $\rks$ on the interval
  $[0,1]$ satisfies this condition and
  theorem~\ref{theorem:implicit:colloction-order} holds true with
  $r=s$. On the other hand $\pi(t)$ is not orthogonal on the
  constants. In this case the theorem holds true with $r=0$.
\end{proof}

\begin{frame}
\begin{theorem}
  \index{Runge-Kutta method!continuous}
  The collocation polynomial $y(t)$, defined through a $\rks$-stage
  collocation method of the form~\eqref{eq:implicit:13}, defines a 
	continuous Runge-Kutta method of order $s$. This means for the difference
	of the exact solution $u(t)$ of the initial value problem and the 
	collocation polynomial $y(t)$ we get the estimate
  \begin{gather}
    \label{eq:implicit:15}
    \norm{u(t)-y(t)} \le C h^{s+1}.
  \end{gather}
  Additionally we obtain for the derivatives of order $k\le s$ the
	estimate
  \begin{gather}
    \label{eq:implicit:16}
    \norm{u^{(k)}(t)-y^{(k)}(t)} \le C h^{s+1-k}.
  \end{gather}
\end{theorem}
\end{frame}

\begin{proof}
  We immediately obtain the statement for the derivatives through
	the fact, that the polynomial $y'(t)$ interpolates the function $u'(t)$ 
	in the points $\rkc_i$. We receive the error estimate for $y(t)$
  by integration of the initial value. 
\end{proof}

\begin{frame}
\begin{definition}
  \defindex{Collocation method!Gauß}
  The $\rks$-stage \define{Gauß-Collocation method} is a method of the 
  form~\eqref{eq:implicit:2}, where the collocation points are a set of
  $\rks$ Gauß points in the interval $[0,1]$. As a Reminder: a
  set of $\rks$ Gauß points in the interval $[0,1]$ consist of roots
  of the $\rks$-th orthogonal polynomial with respect to the inner product
  \begin{gather*}
    \left<p,q\right> = \int_0^1p(t)q(t)\,dt. 
  \end{gather*}
\end{definition}
\end{frame}

\begin{example}
  The two- and three-stage Gauß-Collocation methods were described by
  Hammer and Hollingsworth or\ Kuntzmann and Butcher. 
	Their Butcher tables are (c.f.~\cite[Tables
  7.3, 7.4]{HairerNorsettWanner93}):
  \begin{table}[h]
    \begin{xalignat*}{2}
      &\def\arraystretch{1.5}
      \begin{array}{c|cc}  
        \frac{3-\sqrt3}{6} & \frac14 & \frac14-\frac{\sqrt3}{6}
        \\
        \frac{3+\sqrt3}{6} & \frac14+\frac{\sqrt3}{6} & \frac14
        \\\hline
        & \frac12 & \frac12
      \end{array}
      &&\def\arraystretch{1.4}
      \begin{array}{c|ccc}
        \frac{5-\sqrt{15}}{10}
        & \frac{5}{36}
        & \frac29 - \frac{\sqrt{15}}{15}
        & \frac{5}{36} - \frac{\sqrt{15}}{30}
        \\
        \frac12
        & \frac{5}{36} + \frac{\sqrt{15}}{24}
        & \frac29
        & \frac{5}{36} - \frac{\sqrt{15}}{24}
        \\
        \frac{5+\sqrt{15}}{10}
        & \frac{5}{36} + \frac{\sqrt{15}}{30}
        & \frac29 + \frac{\sqrt{15}}{15}
        & \frac{5}{36}
        \\\hline
        & \frac{5}{18} & \frac49 & \frac{5}{18}
      \end{array}
    \end{xalignat*}
    \caption{Gauß-Collocation methods with two and three collocation points}
    \label{tab:gauss-kollokation-2-3}
  \end{table}
\end{example}

\begin{frame}
\begin{theorem}
  The $\rks$-stage Gauß-Collocation method is consistent of order $2\rks$.
\end{theorem}
\end{frame}


\begin{proof}
  This theorem is a corollar of theorem~\ref{theorem:implicit:colloction-order}.
\end{proof}

\begin{frame}
\begin{theorem}
% HW p. 181 Example 12.3
  Gauß-Collocation methods are \putindex{B-stable}.
\end{theorem}
\end{frame}

\begin{proof}
  Let be $y(t)$ and $z(t)$ the collocation polynomials
  according to~\eqref{eq:implicit:13} with respect to the initial
	values $y_0$
  or $z_0$. Analogous to the proof of theorem~\ref{theorem:implicit:monotonic}
 	we introduce the auxiliary function
  $m(t) = | z(t)-y(t)|^2$. In the collocation points $\xi_i =
  t_0 + \rkc_i h$ it holds
  \begin{multline*}
    m'(\xi_i) = 2 \Re\left<
      z'(t)-y'(t),z(t)-y(t)\right>
    \\
    = 2 \Re\left<f(\xi_i,z(\xi_i)) -
      f(\xi_i,y(\xi_i)),z(t)-y(t)\right> \le 0. 
  \end{multline*}
  Since Gauß quadrature is exact for polynomials of degree $2s-1$, we have:
  \begin{multline*}
    |z_1-y_1|^2 = m(t_0+h) = m(t_0) + \int_{t_0}^{t_0+h} m'(t)\dt
    \\
    = m_0+h \sum_{i=1}^s b_i m'(\xi_i) \le m(t_0) =  |z_0-y_0|^2.
  \end{multline*}
\end{proof}



%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "notes"
%%% End: 
