\chapter{Explicit One-Step Methods and Convergence}
\mode*
\begin{example}[Euler's method]
	Here we want to introduce a first, simple prototype of a method 
	which numerically solves an IVP or rather 
	Volterra's integral equation.
  
	Let's consider the following: given an IVP
	of the form~\eqref{eq:awa} and one wants to calculate
	the value $u(T)$ at a later point in time $T$. 
  
	To that we note first of all that for an IVP at the initial point $t_0$,
	not only the function value $u(t_0) = u_0$ is known, but also the derivative
	$u'(t_0) = f(t_0, u_0)$. Thus we are capable to replace the solution
	$u(t)$ in blue by a straight line $y(t)$ in red, 
	which we can see in the left figure.
  \begin{figure}[htp]
    \centering
    \includegraphics[width=.48\textwidth]{fig/euler1}
    \includegraphics[width=.48\textwidth]{fig/euler2}
    \caption{Derivation of the Euler method. Left: replacement of the
			solution of the IVP by a line with slope and initial point given 
			by the IVP. Right: euler method with three subintervals.}
    \label{fig:explicit:Euler}
  \end{figure}
	The figure suggests that in general the accuracy of this method 
	may not be very good. The first improvement is that we do not
	draw the line through the whole interval from $t_0$ to $T$.
	Instead we insert intermediate points and apply the method 
	to each subinterval, where we use the result of a previous interval
	as the initial point for the next subinterval. 
	As a result one obtains a polygonal chain\footnote{German: Eulersches Polygonzugverfahren}.   
\end{example}

\begin{frame}
\begin{definition}
	To the time interval $I = [t_0,T]$, we define a partitioning 
	in subintervals, also known as \textbf{time steps}.\defindex{Time step}
	Here we we choose the following notation (see also
  figure~\ref{fig:explicit:schritte}):   
  \begin{figure}[tbp]
    \centering\small
    \begin{tikzpicture}
      \draw(0,1) -- node [anchor=south]{$I$} (9,1);
      \draw[thick](0,.9) node[anchor=north]{$t_0$} -- (0,1.1);
      \draw[thick](9,.9) node[anchor=north]{$T$} --(9,1.1);

      \draw(0,0)--(2,0);
      \draw(4,0)-- node [anchor=south]{$I_k$}(5,0);
      \draw(7,0)--(9,0);
      \draw[dotted](2,0)--(4,0);
      \draw[dotted](5,0)--(7,0);
      \draw[thick](0,-.1) node[anchor=north]{$t_0$} -- (0,.12);
      \draw(1,-.1) node[anchor=north]{$t_1$} --(1,.1);
      \draw(2,-.1) node[anchor=north]{$t_2$} --(2,.1);
      \draw(4,-.1) node[anchor=north]{$t_k$} --(4,.1);
      \draw(5,-.1) node[anchor=north]{$t_{k+1}$} --(5,.1);
      \draw(7,-.1) node[anchor=north]{$t_{n-2}$} --(7,.1);
      \draw(8,-.1) node[anchor=north]{$t_{n-1}$} --(8,.1);
      \draw[thick](9,-.1) node[anchor=north]{$t_n=T$} --(9,.12);
    \end{tikzpicture}
    \caption{Partition of the interval $I=[t_0,T]$ in time steps $I_1,\dots,I_n$.}
    \label{fig:explicit:schritte}
  \end{figure}
  the time steps $I_k = [t_{k},t_{k+1}]$ have the step size $h_k = t_{k+1}
  - t_{k}$. 
	An partitioning in $n$ time steps implies $t_n = T$. 
	The term $k$-th time step is used in both the interval $I_k$ and 
	for the point in time $t_k$, but it should always be clear through context
	which one is meant. 
\end{definition}
\end{frame}

\begin{frame}
\begin{definition}
	In the following chapters we will regularly compare the solution 
	of IVP with the results of discretization methods.
	Therefore it is appropriate to say a few words about the used notations
	and symbols.
  
  \defindex{Solution!continuous}
	The solution of the IVP is often called a 
  \textbf{continuous solution} \defindex{Continuous solution}. Thereby the continuity is not 
	so paramount but rather the fact, that the solution 
	is non-discretized 
	and therefore a solution of the ``continuous'' problem.
	Its symbol is in general $u$ and we set as abbreviation 
  \begin{gather*}
    u_k = u(t_k).
  \end{gather*}
  If $u$ is vector-valued we also use the abbreviation
  $u^{(k)}$ or $u_i^{(k)}$ as a entry of the vector $u(t_k)$.

  \defindex{Solution!discrete}
	In general we write the \textbf{discrete solution} \defindex{Discrete solution} with the
  symbol $y$. Analog to above we use $y_k$ or $y^{(k)}$ for the
  value of the discrete solution at the point in time $t_k$. In contrast
	to the continuous solution is $y$ primarily not defined at the intermediate values, i.e. 
	between the time steps.
\end{definition}
\end{frame}

\begin{frame}
\begin{definition}[Explicit one-step method]
  \defindex{One-step method!explicit}
  An \textbf{explicit one-step method} \defindex{Explicit one-step method} is a method which 
	computes in the time steps  $t_0,\dots,t_n$ a sequence of
	approximations $y_1\,\dots,y_n$ for the solution of an IVP.
	The method is called \textbf{one-step method} because the
	value $y_{k+1}$ explicitly depends only of the values $y_k$ 
	and $f(t_k, y_k)$ and not on previous values. 
  
  Explicit\footnote{The adjective
  `explicit' is here in contrast to `implicit'
  one-step methods, where the increment function can depend 
	on $y_{k+1}$ as well.} one-step methods can be written 
	in the form
  \begin{gather}
    \label{eq:explicit:8}
    y_{k+1} = y_k + h_{k} \verfahren_{h_k}(t_k,y_k), 
  \end{gather}
  where $\verfahren()$ is called
  \textbf{increment function} \defindex{Increment function}\footnote{German: Verfahrensfunktion or Inkrementfunktion}. 
	We will often omit the index $h_k$ on $\verfahren_{h_k}()$ because it is clear that the method 
	is always applied to time intervals.
\end{definition}
\end{frame}

\begin{remark}
  \label{remark:expl:first-step}
	For one-step methods every step is \emph{per definition} similar.
	Therefore it is in principle 
	sufficient to observe the first step under consideration of local properties.  
	Hence, in the following we will make statements about the value
	$y_1$ dependent from $y_0$, which then can be transfered to the
	general step from $y_n$ to $y_{n+1}$. Moreover
	we will often restrict ourselves for definitions of methods to
	write
  \begin{gather*}
    y_1 = y_0 + h \verfahren(t_0, y_0).
  \end{gather*}
  This may imply that the values $y_k$ with $k\ge 2$ are computed 
	through	the formula~\eqref{eq:explicit:8} with the respective $h$ and
	the same increment function.
	Thus, we solve a new IVP on each interval. 
\end{remark}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Error analysis}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{example}
  Given the IVP
  \begin{equation*}
    u'  = u, \qquad
    u(0)  = 1. 
  \end{equation*}

  Obviously the solution is $u(t) = e^t$. However we obtain for $h = 1$ with  
	Euler's method the following solution:
  \begin{equation*}\begin{split}
    y_0 & = u_0 = 1 \\
    y_1 & = y_0 + h_0 f(t_0, y_0) = 2 \\
    y_2 & = 4 \\
    y_3 & = 8 \\
    ... \\
  \end{split}\end{equation*}
	One notices that the error is growing. This approximation of the 
	solution can be improved by shrinking $h$.
	On the other hand it occurs the question in which way the (maximal) 
	error is depended on the step size $h$.
\end{example}

\begin{remark}
	In the graphic for Euler's method in figure~\ref{fig:explicit:Euler}, 
	we observe that the error consists of two parts
	at a given time	$t_{k+1}$:
	Firstly it occurs an error on the interval $I_k$ by 
	the replacement of the differential equation through a method.
	Secondly we have to add the error which results because of the fact, that our 
	initial value $y_k$ is already faulty on this interval.
	The following definitions and theorems are dedicated to the analysis 
	of this situation.	
\end{remark}

\begin{frame}
\begin{definition}
  Let $u$ be a solution of the differential equation $u' = f(t,u)$
  at least on the interval $I_k = [t_k, t_{k+1}]$ of length
  $h_k=t_{k+1}-t_{k}$. Then the insertion of the continuous solution
	into the method yields to the \textbf{truncation error}\footnote{German: Abschneidefehler or Diskretisierungsfehler},\defindex{Truncation error}
	 which is defined as
  \begin{gather}
    \label{eq:explicit:6}
    \tau_{k} = \frac1{h_k} \Bigl(u(t_{k+1}) - u(t_{k}) - h_k \verfahren_{h_k}\bigl(
    t_{k}, u(t_k)\bigr)\Bigr).
  \end{gather}
\end{definition}
\end{frame}

\begin{remark}
  The term $h_k \tau_k$ is often called \textbf{local error} \defindex{Local error}.
  It is the error which would occure if one would have executed 
	the method on the interval $I_k$ with the correct initial value.
\end{remark}

\begin{frame}
\begin{definition}[Order of consistency]
  \defindex{Consistency}
  \defindex{Order!of consistency}
  The one-step method $F(h;t,y)$ is consistent of order $p$ with
  the IVP, if the following holds true for the truncation error:
  \begin{gather}
    \label{eq:explicit:3}
    \max\limits_n | \tau_n | \leq c h^p
  \end{gather}
\end{definition}
\end{frame}

\begin{example}[Euler method]
	To find out which order of consistency the Euler method has, we 
	consider the Taylor expansion of the solution at the point $t_n$:
  \begin{equation*}
    u(t_n) = u(t_{n-1}) + h_n u'(t_{n-1}) + \frac12 h_n^2 u''(\xi)
  \end{equation*}

	As a result the trucation error reduces to:

  \begin{equation*}\begin{split}
    \tau_n & = \frac{u(t_n) - u(t_{n-1})}{h_n} - F(h;t_{n-1},u(t_{n-1})) \\
                & = \frac{u(t_{n-1}) + h_n f(t_{n-1},u_{n-1}) + \frac12 h_n^2 u''(\xi) - u(t_{n-1})}{h_n} - f(t_{n-1}; u_{n-1}) \\
                & = \frac12 h_n u''(\xi) \\
  \end{split}\end{equation*}

  \noindent For $u''(\xi)$ however it holds:

  \begin{equation*}
    u''(\xi) = \diffq[\xi] f(\xi,u(\xi)) = \pdiffq[\xi] f(\xi,u(\xi)) + \pdiffq[u] f(\xi,u(\xi)) \underbrace{u'(\xi)}_{= f(\xi,u(\xi))}
  \end{equation*}

  \noindent Under the assumtion that $f \in C^1$ on a compact set around the graph
	of $u$, this term is limited.

  $\Rightarrow | \tau_n | \leq c h_n^1$

  \noindent Thus, the Euler method is consistent of order 1.
\end{example}

\begin{frame}
\begin{lemma}[Discrete Gronwall inequality]
  \index{Gronwall's inequality}
  Let $(w_n)$, $(a_n)$ and $(b_n)$ be non-negative, real
  number sequences with $w_0 \leq b_0$ and $w_n \leq
  \sum\limits_{k=0}^{n-1} a_k w_k + b_n$ for $n \geq 1$. If $b_n$
  is monoton increasing then the following holds true:
  \begin{gather}
    \label{eq:explicit:5}
    w_n \leq \exp \left(\sum\limits_{k=1}^{n-1} a_k\right) b_n
  \end{gather}
\end{lemma}
\end{frame}

\begin{todo}
\begin{proof}
	%We refer to~\cite[Hilfssatz 2.1]{Rannacher12}. (falscher Weg)
  Bitte über die kontinuierliche Ungleichung
\end{proof}
\end{todo}

\begin{frame}
\begin{definition}[Discrete Lipschitz-condition]
  \defindex{Lipschitz condition!discrete}
  A function $F$ fullfills the discrete Lipschitz-condition, if there is a $L > 0$ with
  \begin{gather}
    \label{eq:explicit:4}
    | F(h;t,y) - F(h;t,x) | \leq L | x - y |
  \end{gather}
\end{definition}
\end{frame}

\begin{frame}
\begin{theorem}[Discrete continuity]
  If $F(...)$ fullfills the Lipschitz-condition then the method is discrete stable, i.e. for  
	arbitrary sequences $(y_n)$ and $(z_n)$ it follows

  \begin{equation*}
    | y_n - z_n | \leq e^{L(t-t_0)} (| y_0 - z_0 | + \sum\limits_{k=1}^n h_k | (Ly)_k - (Lz)_k |)
  \end{equation*}
\end{theorem}
\end{frame}

\begin{todo}
\begin{proof}
We refer to~\cite[Satz 2.1]{Rannacher12}.
\end{proof}
\end{todo}

\begin{frame}
\begin{theorem}[Convergence]
  Let the method $F(...)$ be consistent of order $p$ and discrete stable,
	further let be $y_0 = u_0$. Then we have:

  \noindent the method converges with order $p$ in the form

  \begin{equation*}
    | u(t_n) - y_n | \leq c e^{L(t_n - t_0)} h^p \ \ \ \ \ \ h = \max\limits_n h_n
  \end{equation*}
\end{theorem}
\end{frame}

\begin{todo}
\begin{proof}
We refer to~\cite[Satz 2.2]{Rannacher12}.
\end{proof}
\end{todo}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Runge-Kutta methods}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{intro}
	We are searching for methods with which we can solve IVP numerically.
	In fact we are not solving the IVP, but Volterra's integral 
	eqution~\eqref{eq:volterra}.
	Hence we can consider solving differential equations 
	as a quadrature problem.
	There, the difficulty is naturally that the function, which we integrate,
	is not known. 

	This consideration leads to a class of methods to solve IVP,
	the Runge-Kutta methods.
\end{intro}

\begin{frame}
\begin{definition}[Explicit Runge-Kutta methods]
  \defindex{Runge-Kutta method!explicit (ERK)}
  \index{ERK|see{Runge-Kutta method}} 
  A \textbf{explicit Runge-Kutta method (ERK)} is a
  one-step method with the representation
  \label{def:erk}
  \begin{subequations}
    \label{eq:explicit:1}
    \begin{xalignat}{2}
      \label{eq:explicit:1a}
      \rkg_i &= y_0 + h \sum_{j=1}^{i-1} \rka_{ij} k_j
      & i &= 1,\dots,\rks
      \\
      \label{eq:explicit:1b}
      k_i &= f\left(t_0+h \rkc_i, \rkg_i\right)
      & i &= 1,\dots,\rks
      \\
      \label{eq:explicit:1c}
      y_1 &= y_0 + h \sum_{j=1}^{\rks} \rkb_j k_j
    \end{xalignat}
  \end{subequations}
  In this method the values $h\rkc_i$ constitute the quadratur points
  on the interval $[0,h]$. The values $k_i$ are the
  function values of the integrand in this points and $\rkg_i$ consitiute
  estimates for the solution $u(t_0+h\rkc_i)$ in the quatrature points.
\end{definition}
\end{frame}

\begin{remark}
	Pursuant to remark~\ref{remark:expl:first-step} we give here the 
	formula for the calculation of $y_1$ through $y_0$.
	The formula for a later time step $k$ reveals if one replaces    
	$y_0$ and $t_0$ through $y_k$ and $t_k$ to obtain $y_{k+1}$.
\end{remark}

\begin{remark}
	The intermediate values $\rkg_i$ will not be saved separately 
	in typical implementations, because one can already execute the method
	with the values $k_i$ alone. In fact we will see later that for
	the discussion, however, the values $\rkg_i$ can be useful.
\end{remark}

\begin{frame}
\begin{definition}[Butcher's tableau]
  \defindex{Butcher's tableau}
	In the notation of Runge-Kutta methods it is for
	clarification purposes common to write the coefficients of the 
	equation~\eqref{eq:explicit:1} in the following matrixform:
\begin{gather}
  \label{eq:explicit:2}
  \begin{array}{c|ccccc}
    0 & \\
    \rkc_2 & \rka_{21} \\
    \rkc_3 & \rka_{31} & \rka_{32} \\
    \vdots & \vdots & \vdots & \ddots \\
    \rkc_\rks & \rka_{\rks1} & \rka_{\rks2} & \cdots & \rka_{\rks,\rks-1} \\
    \hline
    & \rkb_1 & \rkb_2 & \cdots & \rkb_{\rks-1} & \rkb_\rks \\
  \end{array}
\end{gather}
\end{definition}
\end{frame}

\begin{remark}
	The first row of the tableau is to read in such a manner, that $k_1$
	is computed directly by $f(t_0, y_0)$,
	while in the formulas~\eqref{eq:explicit:1a} and~\eqref{eq:explicit:1b} 
	the coefficients $a_{ij}$ and $c_i$ are set to zero.
  
	The further rows indicate the rule for the computation
	of the further values $k_i$ in each case according to the 
	formulas~\eqref{eq:explicit:1a} and~\eqref{eq:explicit:1b}.

  The last row below the line is then the short form of the 
	formula~\eqref{eq:explicit:1c}.
\end{remark}

\begin{example}[The Euler method] 
  Euler's method \defindex{Euler's method} has the Butcher tableau:
  \begin{gather*}\begin{array}{c|c}
    0 & \\
    \hline
     & 1 \\
  \end{array}\end{gather*}

  That leads to the already known formula:

  \begin{equation*}
    y_{n+1} = y_n + h_n f(t_n, y_n)
  \end{equation*}
\end{example}

\begin{example}[Two-stage methods]
  \defindex{Modificated Euler method}
  \index{Euler's method!modificated}
	Both of the following methods were discussed by Runge in his article
  of 1895~\cite{Runge95}. The \textbf{modificated
    Euler method}
  is a variation of Euler's method of the following form:
  \begin{equation*}
    \begin{split}
      k_1 & = f(t_n,y_n) \\
      k_2 & = f(t_n + \frac12 h_n, y_n + h_n \frac12 k_1) \\
      y_{n+1} & = y_n + h_n k_2
  \end{split}
\end{equation*}

	The so-called Heun method \defindex{Heun's method} of order 2 is characterized through the  
	equation 
  \begin{equation*}
    \begin{split}
      k_1 & = f(t_n, y_n) \\
      k_2 & = f(t_n + h_n, y_n + h_n k_1) \\
      y_{n+1} & = y_n + h_n ( \frac12 k_1 + \frac12 k_2 ) \\
    \end{split}
  \end{equation*}
  You can find the Butcher tableaus for both methods in the following
  table:
  \begin{table}[h]
    \begin{xalignat*}2
      &\begin{array}{c|cc}
        0 & \\
        \frac12 & \frac12 \\
        \hline
        & 0 & 1 \\
  \end{array}
  &&
\begin{array}{c|cc}
    0 & \\
    1 & 1 \\
    \hline
    & \frac12 & \frac12 \\
  \end{array}
    \end{xalignat*}
    \caption{Butcher tableaus of the two-stage explicit methods of Runge. The modificated Euler method (left) and Heun's method of order 2 (right).}
    \label{tab:explicit:2-stage}
  \end{table}
\end{example}

\begin{example}[Three-stage Runge-Kutta method]
  The Runge-Kutta method of 3rd order:

  \begin{gather*}\begin{array}{c|ccc}
    0 & \\
    \frac12 & \frac12 \\
    1 & -1 & 2 \\
    \hline
    & \frac16 & \frac46 & \frac16 \\
  \end{array}\end{gather*}

  \begin{equation*}\begin{split}
    k_1 & = f(t_n, y_n) \\
    k_2 & = f(t_n + \frac12 h_n, y_n + \frac12 h_n k_1) \\
    k_3 & = f(t_n + h_n, y_n - h_n k_1 + 2 h_n k_2) \\
    y_{n+1} & = y_n + h_n ( \frac16 k_1 + \frac46 k_2 + \frac16 k_3 ) \\
  \end{split}\end{equation*}
\end{example}

\begin{example}[The classical Runge-Kutta method]
  The Runge-Kutta method of 4th order:

  \begin{gather*}\begin{array}{c|cccc}
    0 & \\
    \frac12 & \frac12 \\
    \frac12 & 0 & \frac12 \\
    1 & 0 & 0 & 1 \\
    \hline
    & \frac16 & \frac26 & \frac26 & \frac16 \\
  \end{array}\end{gather*}

  \begin{equation*}\begin{split}
    k_1 & = f(t_n, y_n) \\
    k_2 & = f(t_n + \frac12 h_n, y_n + \frac12 h_n k_1) \\
    k_3 & = f(t_n + \frac12 h_n, y_n + \frac12 h_n k_2) \\
    k_4 & = f(t_n + h_n, y_n + h_n k_3) \\
    y_{n+1} & = y_n + h_n ( \frac16 k_1 + \frac26 k_2 + \frac26 k_3 + \frac16 k_4 ) \\
  \end{split}\end{equation*}
\end{example}

\begin{remark}[Maximal orders for explicit RKM]
	The maximal order of an explicit RKM is limited through
	the number of stages. For a explicit RKM of order $p$ 
	one requires $r$ stages, where $p$ and $r$ relate as follows:

  \begin{table}[h]
    \centering
    \begin{tabular}{c|c|c|c|c|c|c}
      p & 1-4 & 5,6 & 7 & 8 & ... & 10 \\
      \hline
      r & p & p+1 & p+2 & p+3 & ... & 17? \\
    \end{tabular}
    \caption{Maximal orders for explicit Runge-Kutta methods in dependency of the number of their stage}
    \label{tab:explicit:maximal-order}
  \end{table}
  
	This order bounds refer to systems of differential equations.
	For a simple equation they may be better. For instance, it exists
	a five-stage method which solves the one dimensional IVP with order 5.

	For $p = 10$ there is only known a method with $r = 17$ until now.
	It is possible that there exists a method that needs less stages, because
	currently no proof for a minimal number of stages is available.	
\end{remark}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Estimate of the truncation error and time step control}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Extrapolation methods}

Since the exact solution of a DE is (in general) not known,
we cannot compute the truncation error \index{Truncation error} directly.
Therefore we search for a possibility to estimate it in stead.

\begin{equation*}\begin{split}
  \tau_{n+1} & = \frac{u(t_{n+1}) - u(t_n)}{h_{n+1}} - F(t,u(t_n)) \\
  \tau_1 & = \frac{u(t_1) - u(t_0)}{h_1} - F(t_0, u(t_0)) = \frac{1}{h_1} (u(t_1) - y_1)
\end{split}\end{equation*}

Our goal is to approximate the error $u(t_1) - y_1$ 
through $\hat{y_1} - y_1$. The $\hat{y_1}$ can be obtained for instance,
if we excecute the same method with half of the step size.

%%% Herleitung

\begin{frame}
\begin{theorem}
	Let $\hat{y_2}$ be the result of a RKM of order $p$ after 2
  steps with step size $h$ and let $y_2$ be the result after a step
	of step size $2h$. Then it holds for the error:

  \begin{equation*}
    u(t_2) - \hat{y_2} = \frac{\hat{y_2} - y_2}{2^p - 1} + O(h^{p+2})
  \end{equation*}

	Moreover, one receives an approximation $\tilde{y_2} = y_2 + \frac{\hat{y_2} - y_2}{2^p - 1}$
	with $u(t_2) - \tilde{y_2} = O(h^{p+2})$.

  Therefore we get an estimation for $\hat{\tau_n}$ of the form:

  \begin{equation*}
    \hat{\tau_n} = \frac1h \frac{\hat{y_2}-y_2}{2^p - 1} + O(h^{p+1})
  \end{equation*}

  This means the estimation converges for $h \to 0$ to the error.
\end{theorem}
\end{frame}

\begin{todo}
\begin{proof}
  See appendix.
\end{proof}
\end{todo}
%%% Beweis?

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Embedded Runge-Kutta methods}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
An alternative to the estimation of the truncation error in Section 2.3.1
through reduction of the step size, would be using methods 
of different orders.
Thus, the difference of the methods can then be used to estimate the truncation error.

An particularly efficient way to get two solutions with different order,
are the embedded Runge-Kutta methods. Here both solutions
are computed with the same function evaluations, but are added up 
differently.

\begin{frame}
\begin{definition}[Embedded Runge-Kutta methods]
	\defindex{Runge-Kutta method!embedded}
	A embedded Runge-Kutta method of order $\rks$ with
  orders of consistence $p$ and $\hat p$ computes two solutions $y$ and
  $\hat y$ with the same function evaluations. For this purpose we will at first
	compute contributions $k_i$ for $i=1,\dots,\rks$ as in the normal RKM 
	of stage $\rks$. The function values at the end of the time step
	result as follows
  \begin{gather}
    \begin{split}
      y_{n+1} &= y_n + h \sum \rkb_i k_i \\
      \hat y_{n+1} &= y_n + h \sum \hat \rkb_i k_i.
    \end{split}
  \end{gather}
	The method for $y$ has in the following consistence order $p$ and the one
  for $\hat y$ order of consistence  $\hat p > p$. For example $\hat p
  = p+1$.
\end{definition}
\end{frame}

\begin{frame}
\begin{definition}
  The Butcher tableau for the embedded method has the form:
  \begin{gather*}
    \begin{array}{c|ccccc}
      0 & \\
      \rkc_2 & \rka_{21} \\
      \rkc_3 & \rka_{31} & b_{32} \\
      \vdots & \vdots & \vdots & \ddots \\
      \rkc_s & \rka_{\rks1} & \rka_{\rks2} & \cdots & \rka_{\rks,\rks-1} \\
      \hline
      & \rkb_1 & \rkb_2 & \cdots & \rkb_{\rks-1} & \rkb_\rks \\
      \hline
      & \hat \rkb_1 & \hat \rkb_2 & \cdots & \hat \rkb_{\rks-1} & \hat \rkb_\rks
    \end{array}  
  \end{gather*}  
\end{definition}
\end{frame}

\begin{remark}
Some short explanations to that: above the first line the 
contributions $k_i$ for $i=1,\dots,\rks$ are computed as usual.
Then one computes out of that the solutions $y_{n+1}$ and $\hat y_{n+1}$  
with two different quadrature formulas.
\end{remark}

\begin{remark}
	Of course the question occures, why one does not use the 
	solution $\hat y_{n+1}$ if it already has a higher order. 
	In fact, one does that for the implementation of the method.
	However note that the estimate for the 
	truncation error only holds for the worse solution $y_{n+1}$.
\end{remark}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Step size control}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{remark}[Adaptiv step size control]
	Based on the embedded RKM a method with adaptive step size control,
	which uses $| \hat{y_1} - y_1
  |$ as estimation for the error, 
	can be derived. For that one proceed as follows:

  Specification: $| \hat{y_1} - y_1 | \leq TOL$

  Compute: $| \hat{y_1} - y_1 | = err = h \tau_1$

  1. If $err \leq TOL$, accept time step.

  2. Compute $h_{opt}$ through
  \begin{equation*}
    h_{opt} = h \left ( \frac{TOL}{err} \right )^{\frac{1}{p+1}}
  \end{equation*}

  3. If $h_{opt} < h$: Repeat time step with $h = h_{opt}$

  Use $h = h_ {opt}$ in the next time step.
\end{remark}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Continuous Runge-Kutta methods}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{todo}
Übernehmen aus~\cite[II.6, p. 188]{HairerNorsettWanner93}
%oder 11.5 p. 176?
\end{todo}

\begin{intro}
	The so far presented RKM generate supporting points between which
	we can interpolate linearly. We now want to increase the order of
	interpolation by using the $\rkg_i$ to compute the values in between
	the supporting points.
	Thereby we have to consider that the errors of the $\rkg_i$ 
	are in general of lower order. Therefore it is generally not sufficient
	to interpolate this values.
	In stead one defines with the help of the $k_i$
  a new RKM, which provides a approximation in each point $\theta \in [0,1]$.
\end{intro}

\begin{frame}
\begin{definition}
  \defindex{Runge-Kutta method!continuous}
	A continuous Runge-Kutta method is a method of the same type as in 
  definition~\ref{def:erk}, for which the coefficients $\rkb_i$ are replaced 
	by continous functions $\rkb_i(\theta)$ on the interval $[0,1]$.
  For this reason the equation~\eqref{eq:explicit:1c} is replaced by
  \begin{equation}
    y(t_0+ \theta h) = y_0 + \sum\limits_{i=1}^{s^*} \rkb_i (\theta) k_i.
  \end{equation}
	Here the stage number $\rks^*$ may be higher than $\rks$. 
	Then additional intermediate values $k_i$ have to be generated.
\end{definition}
\end{frame}

\begin{remark}
  If $\rks^* > \rks$ choose $k_{\rks+1} = k_1 = f(t_n,y_1)$ of the next time step.
\end{remark}

\begin{example}
  For the classical RKM with 4 stages one is able to define a continuous
	interpolation with $\rks^* = \rks$ through the coefficients
  \begin{equation*}
    \begin{split}
      \rkb_1(\theta) & = \theta - \frac32 \theta^2 + \frac23 \theta^3 \\
      \rkb_2(\theta) & = \rkb_3(\theta) = \theta^2 - \frac23 \theta^3 \\
      \rkb_4(\theta) & = - \frac12 \theta^2 + \frac23 \theta^3. \\
    \end{split}
  \end{equation*}
  For this method the error is $y(\theta) - u(\theta) = O(h^3)$.
\end{example}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "notes"
%%% End: 
