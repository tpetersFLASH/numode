\chapter{Initial Value Problems and its Properties}
\label{cha:awa}
\mode*
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction in initial value problems}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}
\begin{definition}[Ordinary differential equations]
  \defindex{Differential equation!ordinary}
  \index{ODE|see{ordinary differential equation}}
	An \defindex{Ordinary differential equation} 
	\textbf{ordinary differential equation} (ODE) is an equation for a function	$u(t)$,
	defined on an interval $I \subset \R$ and with values in the real or 
	complex numbers or in the space $\R^d$ ($\C^d$), of the form
	\begin{gather}
    F\bigl(t, u(t), u'(t), u''(t), \dots, u^{(n)}(t)\bigr) = 0.
  \end{gather}
	Here $F(\ldots)$ denotes an arbitrary function of its arguments.
	The \textbf{order}\defindex{Order!of a differential equation} $n$
	of a differential equation is the highest derivative which occurs.
	If the dimension $d$ of the value range of $u$ is higher than one, 
	we talk about systems of differential equations.  
\end{definition}
\end{frame}  

\begin{remark}
  A differential equation, which is not ordinary, is called partial.
	These are equations or systems of equations, which contain functions of several independent variables 
	and their partial derivatives.
	An ordinary differential equation is always just with respect to one independent 
	variable and its derivatives. Due to the fact that this manuscript just deals with
	ordinary differential equations, the adjective will be omitted in the following.
\end{remark}

\begin{remark}
  \label{remark:order-reduction}
  Each differential equation of order $n$ can be transformed, with introduction of
  additional variables $u_1(t) = u(t)$, $u_2(t) = u'(t)$ to $u_{n+1}(t) =
  u^{(n)}(t)$, in a system of differential equations of first order. 
	This system has the from
  \begin{gather}
    \label{eq:awa:13}
    \begin{pmatrix}
      u_1'(t) - u_2(t) \\
      u_2'(t) - u_3(t) \\
      \cdots\\
      u_{n}'(t) - u_{n+1}(t) \\
      F\bigl(t, u_1(t),\dots,u_{n+1}(t)\bigr)
    \end{pmatrix}
    = 0
  \end{gather}
\end{remark}

\begin{frame}
\begin{definition}[Explicit differential equation]
  % \defindex{Differential equation!explicit}
  % A \textbf{explicit differential equation} \defindex{Explicit differential equation} 
  %       of first order is a equation of the form
  % \begin{align}
  %   \label{eq:awa:ode}
  %     u'(t) &= f(t,u(t))\\
  %     \text{or shorter:}\qquad u'&=f(t,u). \notag
  % \end{align}
  A differential equation of order $n$ is called explicit, if it is of
  the form
  \begin{gather}
	\label{eq:awa:ode}
    u^{(n)}(t) = F\left(t, u(t), u'(t), \ldots, u^{(n-1)}(t)\right)
  \end{gather}
\end{definition}
\end{frame}  

\begin{corollary}
	Every explicit differential equation of higher order can be written as a system 
	of the form~\eqref{eq:awa:ode} with the help of the representation~\eqref{eq:awa:13}. 
\end{corollary}

\begin{frame}
\begin{definition}
  A differential equation of the form~\eqref{eq:awa:ode} is called \textbf{autonomous}, 
	\defindex{Autonomous differential equation} if
	the right hand side $f$ is not explicit dependent on $t$, i.e.
  \begin{gather}
    u'=F(u)
  \end{gather}
\end{definition}
\end{frame}

\begin{frame}
\begin{definition}[Autonomization]
	Each differential equation can be transformed in an autonomous differential equation.
	This is designated as \textbf{autonomization}. \defindex{Autonomization}
  \begin{equation*}
    U = \begin{pmatrix} u \\ t \end{pmatrix},
    \qquad
    F(U) = \begin{pmatrix} f(t,u) \\ 1 \end{pmatrix},
    \qquad
    U' = F(U)
  \end{equation*}

	A method which provides for the autonomous differential equation the same solution 
	as for the original IVP, is called \textbf{invariant under
    autonomization}.
\end{definition}
\end{frame}

Differential equations usually provide sets of solutions from which 
we have to choose a solution. An important selection criteria is setting an initial value
which leads to a well-posed problem. We come back to that later.

\begin{frame}
\begin{definition}[Initial value problem]
  \label{def:awa}
  \index{IVP|see{initial value problem}} Given a point
  $(t_0,u_0)\in \R \times \R^d$. 
	Furthermore let the function $f(t,u)$ with values in $\R^d$ be defined 
	in a neighborhood $I\times U \subset \R\times \R^d$
	of the initial value. 
	Then an
  \textbf{initial value problem} \defindex{Initial value problem} 
	(IVP) is defined as follows: find a function $u(t)$, such that
  \begin{subequations}
    \label{eq:awa}
    \begin{align}
      \label{eq:awa:2}
      u'(t)&=f\bigl(t,u(t)\bigr)
      \\
      \label{eq:awa:3}
      u(t_0)&=u_0
  \end{align}
  \end{subequations}
\end{definition}
\end{frame}

\begin{frame}
\begin{definition}
  \label{def:awa:local solution}
  \defindex{Solution!local} We call a continuously differentiable
	function $u(t)$ with $u(t_0) = 0$ a \textbf{local solution} \defindex{Local solution} of the 
  IVP~\eqref{eq:awa}, if there exists a neighborhood $I$ of the point in time $t_0$
  in which $u$ and further $f(t,u(t))$ is defined
  and if the equation~\eqref{eq:awa:2} holds for all $t\in I$.
\end{definition}
\end{frame}

\begin{remark}
	We introduced the IVP deliberately in a ``local'' form
	because the local solution term is for our purpose the most useful one in general.
	Because of the fact that the neighborhood $I$ in the upper definition can be arbitrary small,
	we will have to deal later as well with the extension to larger intervals.
\end{remark}

\begin{remark}
	Through the substitution of $t\mapsto \tau$ with $\tau = t-t_0$ it is possible to
	transform every IVP at the point $t_0$ to a IVP in point $0$.
	Occasionally, we will take use of that to save paperwork.
\end{remark}

\begin{remark}
	Under the assumption that the right hand side $f$ is continuous in both 
	arguments, the function $u(t)$ is a solution of the 
	initial value problem~\eqref{eq:awa} if and only if it is a solution of  
  \textbf{Volterra's integral equation} \defindex{Volterra's integral equation}
  \begin{gather}
    \label{eq:volterra}
    u(t) = u_0 + \int_{t_0}^t f\bigl(s,u(s)\bigr)\ds.
  \end{gather}
	The formulation as integral equation allows on the other hand
	a more general solution term, because the problem is already 
	well-posed for functions $f(t,u)$, which are just integrable 
	with respect to $t$.
	In that case the solution $u$ would be just absolutely continuous and not 
	continuously differentiable.   
\end{remark}

\begin{remark}
	Both the theoretical analysis of the IVP and the numerical methods 
	(with exception of the BDF methods) in this lecture notes, solve actually 
	never the IVP~\eqref{eq:awa} but always the associated integral 
	equation ~\eqref{eq:volterra}.
\end{remark}

\begin{frame}
\begin{theorem}[Peano]
	\defindex{Peano's theorem}
  \label{satz:peano}
	Let the function $f(t,x)$ be continuous on $\overline{D}$ with 

    $\overline{D}=\{(t,x) \in \R\times\R^d \ |\  |t-t_0|<\alpha, \ |x-u_0|<\beta \}$
  where $\alpha,\beta>0$.

  \noindent Then there exists a solution
    $u(t) \in C^1(I)$
  on the interval
    $I=[t_0-T,t_0+T]$
  with $T=min(\alpha ,\frac{\beta}{M})$
  and $M=\underset{(t,x)\in D}{max} \ |f(t,x)|$
\end{theorem}
\end{frame}

\begin{proof}
  For the proof we refer to \ref{sec:proof-peano} or the lecture notes of Prof. Rannacher
	or other literature of ordinary differential equations.
\end{proof}

\begin{remark}
	The Peano existence theorem does not make any statements about the 
	uniqueness of a solution and also just guarantees local existence.
	We will turn our attention, after a short study of a particularly easy
	case of linear differential equations in section~\ref{sec:awa:well-posedness},
	to this questions in detail.
\end{remark}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Linear differential equation and the Gronwall's inequality}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{intro}
	The examination of linear differential equation turns out to be 
	very simple. Simultaneously the results, which we obtain here,
	will provide us important statements for general non-linear 
	IVP, as we will see later on. Therefore we pay particular attention to the linear special case. 
\end{intro}

\begin{frame}
\begin{definition}
  \defindex{Differential equation!linear}
	A IVP is called \textbf{linear} \defindex{Linear differential equation} according to definition~\ref{def:awa},
	if the right hand side $f$ is linear in $u$. That means we can write it in the form
  \begin{subequations}    
    \label{eq:awa:4}
    \begin{xalignat}{2}
      \label{eq:awa:5}
      u'(t) &= A(t)u(t) + b(t)
      & \forall t &\in \R \\
      \label{eq:awa:6}
      u(t_0) &= u_0
    \end{xalignat}
  \end{subequations}
  with a continuous matrix function $A:\R\to \C^{d \times d}$.
\end{definition}
\end{frame}

\begin{frame}
\begin{definition}
  Let be the matrix function $A:I\to \C^{d\times d}$ continuous. 
	Then the function defined through
  \begin{gather}
    \label{eq:awa:7}
    M(t) = \exp\left(-\int_{t_0}^t A(s) \ds\right)
  \end{gather}
  is called \textbf{integrating factor} \defindex{Integrating factor} of the
  equation~\eqref{eq:awa:5}.
\end{definition}
\end{frame}

\begin{corollary}
  The integrating factor $M(t)$ has the properties
  \begin{align}
    \label{eq:awa:14}
    M(t_0) &= \identity\\
    \label{eq:awa:15}
    M'(t) &= -M(t)A(t).
  \end{align}
\end{corollary}

\begin{frame}
\begin{lemma}
  % Voraussetzungen an A und b
  \label{lemma:awa:factor}
  A solution of the IVP~\eqref{eq:awa:4} is given through the 
  representation
  \begin{gather}
    \label{eq:awa:8}
    u(t) =  M(t)^{-1}\left(u_0 + \int_{t_0}^t M(s) b(s) \ds\right)
  \end{gather}
  with the integrating factor $M(t)$ of the equation~\eqref{eq:awa:7}.
  This solution exists for all $t\in \R$.
\end{lemma}
\end{frame}

\begin{proof}
	We consider the auxiliary function $w(t) = M(t) u(t)$ with the
  integration factor $M(t)$ of the equation~\eqref{eq:awa:7}. From the
  chain rule it follows
  \begin{gather*}
    w'(t) = M'(t) u(t) + M(t) u'(t)
    = -M(t)A(t)u(t) + M(t) u'(t).
  \end{gather*}
  If one compares the right hand side with the differential 
	equation~\eqref{eq:awa:5}, then we will see that $w$ 
	solves the equation
  \begin{gather*}
    w'(t) = M(t) b(t).
  \end{gather*}
	This can be integrated directly to obtain	
  \begin{gather*}
    w(t) = u_0 + \int_{t_0}^t M(t) b(t),
  \end{gather*}
	where we use that $w(t_0) = u_0$.
  According to lemma~\ref{lemma:appendix:exp-1}, about the
  matrix exponential function, \index{Matrix exponential function} $M(t)$ is invertible for all $t$. 
	With the definition of $w(t)$ we are therefore able to solve for $u(t)$,
  which results in the equation~\eqref{eq:awa:8}. The global
	solvability follows from the fact that the solution is defined 
	for arbitrary $t\in \R$.
\end{proof}

\begin{frame}
\begin{lemma}[Gronwall's inequality]
  \label{lemma:Gronwall}
	Let $w(t)$, $a(t)$ and $b(t)$ be piecewise continuous and nonnegative
	functions. Furthermore, let $b(t)$ be monotonically increasing and 
	let $w(t)$ satisfy the integral inequality
  \begin{gather}
    \label{eq:awa:10}
    w(t) \le b(t) + \int_{t_0}^t a(s)w(s)\ds,\qquad t\ge t_.
  \end{gather}
  Then the following holds true for $t \ge t_0$:
  \begin{gather}
    \label{eq:awa:11}
    w(t) \le b(t) \exp\left( \int_{t_0}^t  a(s) \ds\right).
  \end{gather}
\end{lemma}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\begin{todo}[Gronwallsche Ungleichung]
  % \label{lemma:Gronwall}
  % Seien $w(t)$, $a(t)$ und $b(t)$ nichtnegative, messbare Funktionen auf dem
  % Intervall $I=[t_0,T]$. Ferner sei $b(t)$ monoton steigend und es
  % gelte die Integralungleichung
  % \begin{gather}
  %   \label{eq:awa:10}
  %   w(t) \le b(t) + \int_{t_0}^t a(s)w(s)\ds.
  % \end{gather}
  % Dann gilt für $t\in I$:
  % \begin{gather}
  %   \label{eq:awa:11}
  %   w(t) \le b(t) \exp\left(b(s) \int_{t_0}^t  a(s) \ds\right)
  % \end{gather}
%\end{todo}

\begin{proof}
	The proof for continuous functions $w(t)$ uses 
  lemma~\ref{lemma:awa:factor} and is posed as an exercise.
  
	If the functions are only piecewise continuous, then let $\{t_k\}$
	be a set of all points on which one of the functions has a jump.
	Thus the argument for continuous functions can be applied to
	every subinterval $[t_k,t_{k+1}]$, which proves the theorem.
\end{proof}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{remark}
	On the form of the requirements~\eqref{eq:awa:10} as well as the
  estimation~\eqref{eq:awa:11}, we can see that Gronwall's
  inequality is basically based on the construction of a majorant for 
	$w(t)$, which satisfies a linear IVP.
\end{remark}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{corollary}
  \label{corollary:awa:unique-linear}
  Let the functions $u(t)$ and $v(t)$ be two solutions of the linear
  differential equation~\eqref{eq:awa:5}. If both functions
  coincide in a point $t_0$ then they are identical.
\end{corollary}

\begin{proof}
	The difference $w(t) = v(t) - u(t)$ solves the integral equation
  \begin{gather*}
    w(t) = \int_{t_0}^t A(s) w(s) \ds.
  \end{gather*}
  Hence $|w(t)|$ satisfies the integral inequality
  \begin{gather*}
    |w(t)| \le \int_{t_0}^t |A(s)| |w(s)| \ds,
  \end{gather*}
	from which we conclude with Gronwall's inequality~\eqref{eq:awa:11} 
	for $b(t) = 0$, that $|w(t)|=0$ for all $t$ and therefore
  $u(t) = v(t)$.
\end{proof}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\begin{lemma}
  \index{Homogeneous}
  \index{Differential equation!linear!homogeneous}
  The solutions of the homogeneous, linear differential equation
  \begin{gather}
    \label{eq:awa:9}
    u'(t) = A(t) u(t)
  \end{gather}
  with $u:\R\to\R^d$, define a vector space of dimension $d$. Let
  $\{\psi^{(i)}\}_{i=1,\dots,d}$ be a basis of $\R^d$. 
	Then the solutions $\phi^{(i)}(t)$ of the equation~\eqref{eq:awa:9} with 
  initial values $\phi^{(i)}(0) = \psi^{(i)}$ form a basis of the solution
  space. The vectors $\{\phi^{(i)}(t)\}$ are linear independent
  for all $t\in \R$.
\end{lemma}
\end{frame}

\begin{proof}
	At first we observe that for two solutions
	$u(t)$ and $v(t)$ of the equation~\eqref{eq:awa:9},
	its sum and its scalar multiple are solutions too,
	due to linearity \index{Linearity} of 
	the derivative and the right hand side.  
	Therefore the vector space structure is proven.
 
  Let now $\phi^{(i)}(t)$ are solutions of the IVP with linear
	independent initial values $\{\psi^{(i)}\}$.
  As a consequence the functions are linear independent as well.
  Assume that $w(t)$ is a solution of the
  equation~\eqref{eq:awa:9}, which cannot be written as a linear
	combination of $\psi^{(i)}$.
	Then $w(0)$ is not a linear combination of the vectors $\psi^{(i)}$: 
	would it be one, let's say $w(0) = \sum \alpha_i \psi^{(i)}$,
	then $w(t) = \sum
  \alpha_i \phi^{(i)}(t)$ would be a linear combination because of 
	corollary~\ref{corollary:awa:unique-linear}. 	
	Since $\{\psi^{(i)}\}$ is according to the assumtions a basis of $\R^d$, 
	such a $w(0)$ cannot exist.
	Hence it is shown	that $\phi^{(i)}(t)$ is a basis of the
	solution space of dimension $d$. 
  
	It remains to show that the $\phi^{(i)}(t)$ are linear independent 
	for all $t\in \R$.
	We assume that the set $\phi^{(i)}(t)$ is linear dependent for a 
	value $t_1$.  	
	Then the following holds true without loss of generality (w.l.o.g.)
  \begin{gather*}
    \phi^{(d)}(t_1) = \sum_{i=1}^{d-1}\alpha_i\phi^{(i)}(t_1) =: w(t).
  \end{gather*}
	Again according to corollary~\ref{corollary:awa:unique-linear} we have  
  $\phi^{(d)} \equiv w$, moreover $\phi^{(d)}(0) = w(0)$ which again
 	stands in contradiction to the assumption $\psi^{(d)}$ is a linear combination 
	of the other initial values.
\end{proof}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\begin{definition}
  \label{def:fundamentalmatrix}
  A basis $\{\phi^{(1)},\dots,\phi^{(d)}\}$ of the solution space of the
  linear differential equation~\eqref{eq:awa:9}, moreover the basis of the
  initial values $\phi^{(i)}(0) = e_i$, is called \textbf{fundamental system} \defindex{Fundamental system}. 
  The matrix function
  \begin{gather}
    \label{eq:awa:12}
    \fundam(t) =
    \begin{pmatrix}
      \phi^{(1)}(t)\dots\phi^{(d)}(t)
    \end{pmatrix}
  \end{gather}
  with column vectors $\phi^{(i)}(t)$ is called
  \textbf{fundamental matrix} \defindex{Fundamental matrix}.
\end{definition}
\end{frame}

\begin{corollary}
	The fundamental matrix is regular for all $t\in \R$ and  solves the IVP
  \begin{align*}
    \fundam'(t) &= A(t)\fundam(t)\\
    \fundam (0) &= \identity.
  \end{align*}
\end{corollary}

\begin{frame}
\begin{lemma}
  \label{lemma:linear-inhomogen}
  Each solution of the inhomogeneous, linear IVP~\eqref{eq:awa:4} can
	be written as a linear combination of the column vectors of the 
	fundamental matrix $\fundam$ and an arbitrary solution of the 
	inhomogeneous equation~\eqref{eq:awa:5}.
\end{lemma}
\end{frame}

\begin{todo}  
  \begin{proof}
  \end{proof}
\end{todo}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Well-posedness of the IVP}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\label{sec:awa:well-posedness}

\begin{frame}
\begin{definition}
  \label{def:Hadamard} 
	A mathematical task is called \textbf{well-posed} \defindex{Well-posedness} if
  the following \textbf{Hadamard conditions} are satisfied:
  \index{Hadamard conditions}
  \begin{enumerate}
  \item A solution exists.
  \item The solution is unique.
  \item The solution is continuously dependent on the data.
  \end{enumerate}
\end{definition}
\end{frame}

\begin{frame}
\begin{definition}
  The function $f(t,y)$ satisfies on its domain $D = I\times\Omega \subset
  \R \times \R^d$ an uniformly continuous \define{Lipschitz condition} if 
	it is Lipschitz continuous with regard to $y$, i.e., it exists a 
	positive constant $L$, such that
  \begin{gather}
    \label{eq:awa:1}
    \forall t\in I;\,x,y\in\Omega \quad:\quad |f(t,x)-f(t,y)| \leq L |x-y|
  \end{gather}

	It satisfies a local Lipschitz condition if the same holds true for all 
	compact subsets of $D$.
\end{definition}
\end{frame}

\begin{frame}
\begin{theorem} [Picard-Lindelöf or Cauchy-Lipschitz]
  \label{satz:picardlindeloef}
	\defindex{Picard-Lindelöf's theorem} 
	Given $f(t,y)$ continuous on a cylinder $D = \{ (t,y) \in \R \times
  \R^d | \ |t-t_0| \leq a, |y-u_0| \leq b \}$ and satisfies there the
  Lipschitz condition ~\eqref{eq:awa:1}. Then the
  IVP~\eqref{eq:awa} is uniquely solvable on the interval $I =
  [t_0-T,t_0+T]$ where $T = min \{ a, \frac{b}{M} \}, M =
  \underset{D}{max} \norm{f}$.
\end{theorem}
\end{frame}

\begin{proof}
	We observe the operator	$F(u)$ which is defined through 
  \putindex{Volterra's integral equation}~\eqref{eq:volterra} as
  \begin{gather}
    \label{eq:awa:16}
   F(u)(t) = u_0 + \int\limits_{t_0}^t f(s,u(s)) \ds.
  \end{gather}
	Obviously $u$ is a solution of Volterra's
  integral equation~\eqref{eq:volterra} if and only if $u$ is a
  fix point \index{Fix point} of $F$ i.e., $u=Fu$.
  
	We now show that $F$ is a contraction under the assumtions of the theorem. 
	Therefore we conclude from the representation
  in~\cite[\S117]{Heuser86} and choose on the space $\mathcal C(I)$, which is the space of the 
	continuous functions on $I$, the norm
  \begin{gather*}
    |u|_e := \underset{t \in I}{max} ~e^{-2 L t} |u(t)|.
  \end{gather*}
  
	With estimating the difference of operator $F$ applied to two functions:
  \begin{align*}
    |F(u)(t) - F(v)(t)|
    & = \left| u_0 - u_0 + \int\limits_{t_0}^t (f(s,u(s)) - f(s,v(s))) \ds \right| \\
    & \leq \int\limits_{t_0}^t \left| f\bigl(s,u(s)\bigr) - f\bigl((s,v(s)\bigr) \right| \ds \\
    & \leq \int\limits_{t_0}^t L |u(s) - v(s)| \underbrace{e^{-2 L s} e^{2 L s}}_{= 1} \ds \\
    & \leq L |u-v|_e \int\limits_{t_0}^t e^{2 L s} \ds \\
    & = L |u-v|_e \frac{e^{2 L t}}{2 L} \\
    & \le \frac12 e^{2 L t}|u-v|_e
  \end{align*}

	tt follows   
  \begin{gather*}
    e^{-2 L t}|F(u)(t) - F(v)(t)| \le \frac12 |u-v|_e,
  \end{gather*}
  for all $t$ and we observe:
  \begin{gather*}
    |F(u)(t) - F(v)(t)|_e \le \frac12 |u-v|_e.
  \end{gather*}
  
	With that we have shown that $F$ is a contraction on the space
  of the continuous functions with the norm $|.|_e$. 
	Due to \putindex{Banach's fixed-point theorem}, 
	$F$ has exactly one fix point, which proves the theorem.
\end{proof}

\begin{remark}
	The norm $|u|_e$ had been chosen with regard to Gronwall's inequaltity,
  which was not used in the proof explicitly. 
	It is equivalent to the norm $|u|_\infty$ because $e^{-2 L t}$ is strictly positiv
  and bounded. On the other hand one could have performed the proof with 
	some more calculations with respect to the ordinary Tchebychev distance (maximum norm) $|u|_\infty$.
\end{remark}

\begin{remark}
	Currently our solution is restricted to $I = [t_0 - T, t_0 + T]$.
	Since $T$ is chosen in such a way in equation~\ref{satz:picardlindeloef} 
  that the graph of $u$ does not leave the domain, this extension always ends  
	on the boundary of $D$. One can
  now extend the solution by solving the next IVP
  $\left\{\begin{array}{l}
      u' = f(t,u)\\
      u(t_1) = u_1\\
  \end{array}\right\}$
	on the interval $I_1$. This way one obtains a solution on
 $I \cup I_1 \cup I_2 \cup ...$.
\end{remark}

\begin{todo}
  \begin{frame}
    \begin{theorem}[Stabilitätssatz]
  \end{theorem}
\end{frame}
\end{todo}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Examples}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{example}
	It exists a solution of the IVP
    $\left\{\begin{array}{l}
      u'=\sin(u)\\
      u(0)=7\\
    \end{array}\right\}$?

  \begin{enumerate}
    \item The IVP is autonomous.
    \item $|\sin(u)| \leq 1$
    \item $\sin(u)$ satisfies the Lipschitz condition with $L=1$.
    \item $D = \R \times \R$
  \end{enumerate}

  $\Rightarrow$ Due to the theorems of Peano~\eqref{satz:peano} and Picard-Lindelöf~\eqref{satz:picardlindeloef} it follows:
  $\exists! u : \R \to \R$
\end{example}

\begin{example}
  $\left\{\begin{array}{l}
    u'(t) = -u^2\\
    u(-1) = 1\\
  \end{array}\right\}$

  \begin{enumerate}
    \item $u^2$ is continuous in $\R$.
    \item However we just have a local Lipschitz condition.
  \end{enumerate}
  \noindent $\Rightarrow$ $u(t) = \frac1t$ auf $(-\infty,0)$
\end{example}

\begin{example}
  $\left\{\begin{array}{l}
    u' = \lambda u\\
    u(0) = u_0\\
  \end{array}\right\}$

	\begin{enumerate}
  \item $\lambda u$ is Lipschitz continuous on $\R$
	\end{enumerate}

  \noindent $\Rightarrow e^{\lambda t} u_0$ is the unique solution in $\R$.
\end{example}


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "notes"
%%% End: 
